﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Omega.Classes
{
    public class Config : INotifyPropertyChanged
    {
        public string Theme { get; set; } = "BaseDark";
        public string Accent { get; set; } = "Indigo";

        public string GameDirectory { get; set; }
        public string WorkDirectory { get; set; }

        public bool OpenAutomatically { get; set; } = true;
        public bool EnableAnimations { get; set; } = true;

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}