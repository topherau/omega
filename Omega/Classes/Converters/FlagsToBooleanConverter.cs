﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Omega.Classes.Converters
{
    public class FlagsToBooleanConverter : IValueConverter
    {
        private long _target;

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            _target = System.Convert.ToInt64(value);
            return (_target & System.Convert.ToInt64(parameter)) != 0;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var setFlag = System.Convert.ToBoolean(value);
            var longParam = System.Convert.ToInt64(parameter);

            if (setFlag)
                _target |= longParam;
            else
                _target ^= longParam;

            var result = Enum.Parse(targetType, _target.ToString());
            return result;
        }
    }
}