﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Omega.Classes.Converters
{
    public class InvertableBooleanToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is bool boolValue))
                return Visibility.Visible;

            if(!bool.TryParse((string)parameter, out var invertParameter) || invertParameter == false)
                return boolValue ? Visibility.Visible : Visibility.Collapsed; // Return standard value

            // Return inverted value
            return boolValue ? Visibility.Collapsed : Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}