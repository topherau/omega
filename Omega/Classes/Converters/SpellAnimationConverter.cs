﻿using System;
using System.Globalization;
using System.Windows.Data;
using Omega.FFX;

namespace Omega.Classes.Converters
{
    public class SpellAnimationConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return $"{value:X4} : {FFXStrings.SpellAnimations[(ushort)value]}";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}