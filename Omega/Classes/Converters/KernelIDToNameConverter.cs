﻿using System;
using System.Globalization;
using System.Windows.Data;
using FFXLib.FileSystem.Data.Records.FFX.Kernel;
using Omega.FFX;

namespace Omega.Classes.Converters
{
    public class KernelIDToNameConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is ushort ushortValue))
                return "Unknown";

            var kernelPage = (ushortValue & 0xF000) >> 12;
            var kernelId = ushortValue & 0x0FFF;

            if (!(parameter is Type typeParameter))
            {
                // Fetch by page
                if (kernelPage == 0 && kernelId == 0)
                    return "(None)";

                switch (kernelPage)
                {
                    case 2:
                        return FFXKernel.ItemDictionary[ushortValue];
                    case 3:
                        return FFXKernel.CommandDictionary[ushortValue];
                    case 4:
                    case 6:
                        return FFXKernel.MonMagicDictionary[ushortValue];
                    case 8:
                        return FFXKernel.AutoAbilityDictionary[ushortValue];
                }
            }
            else
            {
                // Fetch by type
                if (typeParameter == typeof(Panel))
                    return FFXKernel.Nodes[kernelId].Name;

                if (typeParameter == typeof(Item))
                    return FFXKernel.Items[kernelId].Name;

                if (typeParameter == typeof(AutoAbility))
                    return FFXKernel.AutoAbility[kernelId].Name;
            }

            return "Unknown";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}