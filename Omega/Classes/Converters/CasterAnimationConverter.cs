﻿using System;
using System.Globalization;
using System.Windows.Data;
using Omega.FFX;

namespace Omega.Classes.Converters
{
    public class CasterAnimationConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return $"{value:X2} : {FFXStrings.CasterAnims[(byte)value]}";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}