﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Omega.Classes.Converters
{
    public class ByteToBooleanConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is byte byteValue))
                return false;

            return byteValue == 1;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is bool boolValue))
                return 0;

            return boolValue ? 1 : 0;
        }
    }
}