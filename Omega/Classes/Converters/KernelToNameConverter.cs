﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows.Data;
using FFXLib.FileSystem.Data.Records.FFX.Kernel;
using Omega.FFX;

namespace Omega.Classes.Converters
{
    public class KernelToNameConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return string.Empty;

            if (value is Item item)
            {
                return item.Name;
            }
            
            if (value is Sphere sphere)
            {
                var sphereItem = FFXKernel.Items.FirstOrDefault(
                    i => i.SphereType == FFXKernel.Spheres.IndexOf(sphere));
                if (sphereItem != null)
                    return sphereItem.Name;
            }

            if (value is Kaizou customization)
            {
                if (FFXKernel.AutoAbilityDictionary.ContainsKey(customization.AutoAbilityId))
                    return FFXKernel.AutoAbilityDictionary[customization.AutoAbilityId];
            }
            
            return "(Unknown)";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}