﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Media.Imaging;
using FFXLib;

namespace Omega.Classes.SphereGrid
{
    public class GridNode : INotifyPropertyChanged
    {
        public Grid Grid { get; }
        public int Id => Grid.Nodes.IndexOf(this);
        
        public short X { get; set; }
        public short Y { get; set; }
        public ushort NodeType { get; set; }

        public GridGlyph Glyph { get; set; }
        public Dictionary<GridLine, GridNode> LinkedNodes { get; set; }

        public GridNode(Grid grid)
        {
            Grid = grid;
        }

        public override string ToString()
        {
            return $"Node {Id}";
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}