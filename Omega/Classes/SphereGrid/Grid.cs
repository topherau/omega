﻿using System.Collections.Generic;
using System.Linq;
using FFXLib.FileSystem.Data;
using FFXLib.FileSystem.Data.Records.FFX.SphereGrid;
using Omega.FFX;

namespace Omega.Classes.SphereGrid
{
    public class Grid
    {
        public GridType Type { get; }
        public List<GridGlyph> Glyphs { get; private set; }
        public List<GridNode> Nodes { get; private set; }
        public List<GridLine> Lines { get; private set; }

        public Grid(GridType type, SphereGridFile sphereGridFile)
        {
            Type = type;
            ReadGlyphs(sphereGridFile.Glyphs);
            ReadNodes(sphereGridFile.Nodes);
            ReadLines(sphereGridFile.Lines);
        }

        public Grid(GridType type, IEnumerable<Glyph> glyphs, IEnumerable<Node> nodes, IEnumerable<Line> lines)
        {
            Type = type;
            ReadGlyphs(glyphs);
            ReadNodes(nodes);
            ReadLines(lines);
        }

        private void ReadGlyphs(IEnumerable<Glyph> glyphs)
        {
            Glyphs = new List<GridGlyph>();
            foreach (var glyph in glyphs)
            {
                Glyphs.Add(new GridGlyph(this)
                {
                    X = glyph.X,
                    Y = glyph.Y,
                    Type = glyph.Type
                });
            }
        }

        private void ReadNodes(IEnumerable<Node> nodes)
        {
            Nodes = new List<GridNode>();
            foreach (var node in nodes)
            {
                var gridNode = new GridNode(this)
                {
                    X = node.X,
                    Y = node.Y,
                    NodeType = node.DefaultType < 255 && node.DefaultType >= FFXKernel.Nodes.Count
                        ? (ushort) 0
                        : node.DefaultType,
                    Glyph = node.AttachedGlyph == 0xFFFF ? null : Glyphs[node.AttachedGlyph],
                    LinkedNodes = new Dictionary<GridLine, GridNode>()
                };
                Nodes.Add(gridNode);
                node.PropertyChanged += (sender, args) =>
                {
                    if (args.PropertyName == "DefaultType")
                        gridNode.NodeType = node.DefaultType;
                };

                if (node.AttachedGlyph != 0xFFFF)
                    Glyphs[node.AttachedGlyph].AttachedNodes.Add(gridNode);
            }
        }

        private void ReadLines(IEnumerable<Line> lines)
        {
            Lines = new List<GridLine>();
            foreach (var line in lines)
            {
                var gridLine = new GridLine(this)
                {
                    FromNode = Nodes[line.Node1],
                    ToNode = Nodes[line.Node2],
                    CenterNode = line.CenterNode == 0xFFFF ? null : Nodes[line.CenterNode]
                };

                Lines.Add(gridLine);

                gridLine.FromNode.LinkedNodes[gridLine] = gridLine.ToNode;
                gridLine.ToNode.LinkedNodes[gridLine] = gridLine.FromNode;
            }
        }

        public List<GridNode> ShortestPath(GridNode fromNode, GridNode toNode, int lockLevel)
        {
            var vertices = Nodes.ToDictionary(n => n,
                n => n.LinkedNodes.ToDictionary(l => l.Value,
                    l => GetLockLevel(l.Value) > lockLevel ? int.MaxValue : 1));

            var previous = new Dictionary<GridNode, GridNode>();

            List<GridNode> path = null;

            // Set the initial distance for the start node to zero, every other node to infinity
            var distances = vertices.ToDictionary(v => v.Key, v => v.Key == fromNode ? 0 : int.MaxValue);
            var nodes = vertices.Select(v => v.Key).ToList();

            while (nodes.Count != 0)
            {
                nodes.Sort((x, y) => distances[x] - distances[y]);

                var smallest = nodes[0];
                nodes.Remove(smallest);

                if (smallest == toNode)
                {
                    path = new List<GridNode>();
                    while (previous.ContainsKey(smallest))
                    {
                        path.Add(smallest);
                        smallest = previous[smallest];
                    }

                    break;
                }

                if (distances[smallest] == int.MaxValue)
                {
                    break;
                }

                foreach (var neighbor in vertices[smallest])
                {
                    var alt = distances[smallest] + neighbor.Value;
                    if (alt < distances[neighbor.Key])
                    {
                        distances[neighbor.Key] = alt;
                        previous[neighbor.Key] = smallest;
                    }
                }
            }

            path?.Add(fromNode);

            return path;
        }

        public List<GridNode> CalculateBranches(GridNode sourceNode, int lockLevel = 0)
        {
            var searchedNodes = new List<GridNode>();
            var queuedNodes = new Queue<GridNode>();
            queuedNodes.Enqueue(sourceNode);

            while (queuedNodes.Count != 0)
            {
                var currentNode = queuedNodes.Dequeue();

                searchedNodes.Add(currentNode);

                var validNodes = currentNode.LinkedNodes.Where(l => !searchedNodes.Contains(l.Value))
                    .Select(l => l.Value);

                foreach (var node in validNodes)
                {
                    var nodeLockLevel = GetLockLevel(node);
                    if (nodeLockLevel > lockLevel)
                        continue;

                    queuedNodes.Enqueue(node);
                }
            }

            return searchedNodes;
        }

        private int GetLockLevel(GridNode node)
        {
            if (node.NodeType >= 255)
                return 255;

            if (node.NodeType > FFXKernel.Nodes.Count)
                return 0;

            var typeInfo = FFXKernel.Nodes[node.NodeType];
            if (typeInfo.Icon == 16)
                return 4;
            if (typeInfo.Icon == 0)
                return 3;
            if (typeInfo.Icon == 17)
                return 2;
            if (typeInfo.Icon == 18)
                return 1;

            return 0;
        }

        public GridLine AddLine(GridNode fromNode, GridNode toNode, GridNode centerNode = null)
        {
            var line = new GridLine(this)
            {
                FromNode = fromNode,
                ToNode = toNode,
                CenterNode = centerNode
            };

            Lines.Add(line);

            return line;
        }

        public void DeleteLine(GridLine line)
        {
            line.ToNode.LinkedNodes.Remove(line);
            line.FromNode.LinkedNodes.Remove(line);
            Lines.Remove(line);
        }
    }
}