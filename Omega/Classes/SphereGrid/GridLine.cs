﻿namespace Omega.Classes.SphereGrid
{
    public class GridLine
    {
        public Grid Grid { get; }
        public int Id => Grid.Lines.IndexOf(this);

        public GridNode FromNode { get; set; }
        public GridNode ToNode { get; set; }
        public GridNode CenterNode { get; set; }

        public GridLine(Grid grid)
        {
            Grid = grid;
        }

        public override string ToString()
        {
            return $"Line {Id}";
        }
    }
}