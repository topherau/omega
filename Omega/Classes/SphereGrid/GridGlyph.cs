﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using FFXLib;

namespace Omega.Classes.SphereGrid
{
    public class GridGlyph : INotifyPropertyChanged
    {
        public Grid Grid { get; }
        public int Id => Grid.Glyphs.IndexOf(this);

        public ushort Type { get; set; }
        public short X { get; set; }
        public short Y { get; set; }
        public List<GridNode> AttachedNodes { get; } = new List<GridNode>();

        public GridGlyph(Grid grid)
        {
            Grid = grid;
        }

        public override string ToString()
        {
            return $"Glyph {Id}";
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}