﻿using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Controls;
using FFXLib.Configuration;
using MahApps.Metro;
using Omega.Classes;
using Omega.Windows;

namespace Omega
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        public static MainWindow Window { get; private set; }
        public static Config Config { get; private set; }

        private void App_OnStartup(object sender, StartupEventArgs e)
        {
            Config = new ConfigFile<Config>("settings.json", true).Configuration;

            if (Config.Theme == null || ThemeManager.GetAppTheme(Config.Theme) == null)
                Config.Theme = "BaseDark";

            if (Config.Accent == null || ThemeManager.GetAccent(Config.Accent) == null)
                Config.Accent = "Blue";

            ThemeManager.ChangeAppStyle(Current,
                ThemeManager.GetAccent(Config.Accent),
                ThemeManager.GetAppTheme(Config.Theme));

            MainWindow = new MainWindow();
            MainWindow.Show();
        }

        private void FlagsContextMenu_OnClick(object sender, RoutedEventArgs e)
        {
            if (!(e.OriginalSource is MenuItem menuItem))
                return;

            if (!(menuItem.Parent is ContextMenu contextMenu))
                return;

            if (!(contextMenu.PlacementTarget is TextBox textBox))
                return;

            // Retrieve the binding from the text box
            var binding = textBox.GetBindingExpression(TextBox.TextProperty);
            if (binding == null)
                return;

            // Get binding property from binding
            var property = binding.DataItem.GetType().GetProperty(binding.ResolvedSourcePropertyName);

            if (property == null)
                return;

            // Get value from binding object
            var value = property.GetValue(binding.DataItem);

            if (!(value is byte ||
                  value is ushort ||
                  value is uint ||
                  value is ulong))
                return;
            
            var flagsWindow = new FlagsWindow(MainWindow, value, property.Name);
            if (!flagsWindow.ShowDialog() ?? true)
                return;

            property.SetValue(binding.DataItem, flagsWindow.ReturnValue);
        }
    }
}