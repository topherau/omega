﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using FFXLib;
using FFXLib.FileSystem.Data.Records.FFX.Battle;
using FFXLib.FileSystem.Data.Records.FFX.Kernel;
using Microsoft.Win32;
using Microsoft.WindowsAPICodePack.Dialogs;
using Omega.FFX;
using Omega.Windows;

namespace Omega.Panels.Battle
{
    /// <summary>
    /// Interaction logic for MonsterEditor.xaml
    /// </summary>
    public partial class MonsterEditor : INotifyPropertyChanged
    {
        public Monster SelectedKernel { get; set; }
        public Monster SelectedMonster { get; set; }
        public MonsterLoot SelectedLoot { get; set; }

        public MonsterEditor()
        {
            InitializeComponent();

            EnemiesList.SelectedIndex = 0;
        }

        private void EnemiesList_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (EnemiesList.SelectedIndex == -1)
                return;

            var di = FFXKernel.MonsterDictionary.Keys.ToList().IndexOf((ushort) EnemiesList.SelectedValue);
            SelectedKernel = FFXKernel.Monster[di];
            SelectedMonster = FFXBattle.Monster[SelectedKernel.MonsterId];
            SelectedLoot = FFXBattle.MonsterLoot[SelectedKernel.MonsterId];
        }

        private void FilterText_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            EnemiesList.ItemsSource = FilterText.Text == string.Empty
                ? FFXKernel.MonsterDictionary //            Unfiltered list
                : FFXKernel.MonsterDictionary.Where(i => // Filter by name
                    i.Value.IndexOf(FilterText.Text, StringComparison.CurrentCultureIgnoreCase) != -1);

            EnemiesList.SelectedItem = SelectedMonster;
        }

        private void FilterText_OnKeyDown(object sender, KeyEventArgs e)
        {
            // Clear filter when Escape is pressed
            if (e.Key == Key.Escape)
                FilterText.Text = string.Empty;
        }

        private void ButtonAbility_Click(object sender, RoutedEventArgs e)
        {
            var binding = ((FrameworkElement) e.OriginalSource).GetBindingExpression(ContentProperty);
            if (binding == null)
                return;

            var abilityList = (ObservableCollection<ushort>) binding.ResolvedSource;
            var abilityIndex = int.Parse(binding.ResolvedSourcePropertyName.Trim('[', ']'));

            var win = new LookupWindow(new List<LookupList>
            {
                new LookupList
                {
                    Title = "Enemy abilities",
                    List = FFXKernel.MonMagicDictionary.ToDictionary(i => (int) i.Key, i => i.Value)
                },
                new LookupList
                {
                    Title = "Commands",
                    List = FFXKernel.CommandDictionary.ToDictionary(i => (int) i.Key, i => i.Value)
                },
                new LookupList
                {
                    Title = "Items",
                    List = FFXKernel.ItemDictionary.ToDictionary(i => (int) i.Key, i => i.Value)
                }
            }, abilityList[abilityIndex]);

            if (!win.ShowDialog() ?? true)
                return;

            abilityList[abilityIndex] = win.UShortValue;
        }

        private void ButtonAbility_RightClick(object sender, MouseButtonEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void ButtonAutoAbility_Click(object sender, RoutedEventArgs e)
        {
            var binding = ((FrameworkElement) e.OriginalSource).GetBindingExpression(ContentProperty);
            if (binding == null)
                return;

            var abilityList = (ObservableCollection<ushort>) binding.ResolvedSource;
            var abilityIndex = int.Parse(binding.ResolvedSourcePropertyName.Trim('[', ']'));

            var win = new LookupWindow(new List<LookupList>
            {
                new LookupList
                {
                    List = FFXKernel.AutoAbilityDictionary.ToDictionary(i => (int) i.Key, i => i.Value)
                }
            }, abilityList[abilityIndex]);

            if (!win.ShowDialog() ?? true)
                return;

            abilityList[abilityIndex] = win.UShortValue;
        }

        private void DropRateSlider_OnValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            DropRateText.Text = $"{DropRateSlider.Value / DropRateSlider.Maximum:P2}";
        }

        private void ButtonItem_Click(object sender, RoutedEventArgs e)
        {
            var binding = ((FrameworkElement) e.OriginalSource).GetBindingExpression(ContentProperty);
            if (binding == null)
                return;

            var monsterLoot = (MonsterLoot) binding.ResolvedSource;
            var property = monsterLoot.GetType().GetProperty(binding.ResolvedSourcePropertyName);

            var win = new LookupWindow(new List<LookupList>
            {
                new LookupList
                {
                    List = FFXKernel.ItemDictionary.ToDictionary(i => (int) i.Key, i => i.Value)
                }
            }, (ushort) property.GetValue(monsterLoot));

            if (!win.ShowDialog() ?? true)
                return;

            property.SetValue(monsterLoot, win.UShortValue);
        }

        private PropertyInfo GetBindingProperty(object obj, DependencyProperty property)
        {
            if (!(obj is FrameworkElement element))
                return null;

            var binding = element.GetBindingExpression(property);

            return binding?.DataItem.GetType().GetProperty(binding.ResolvedSourcePropertyName);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class ScriptItem
    {
        public uint Offset { get; set; }
        public string Instruction { get; set; }
        public string Comment { get; set; }
    }
}