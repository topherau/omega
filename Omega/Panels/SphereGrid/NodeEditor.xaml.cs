﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Omega.FFX;
using Omega.Windows;
using Panel = FFXLib.FileSystem.Data.Records.FFX.Kernel.Panel;

namespace Omega.Panels.SphereGrid
{
    /// <summary>
    /// Interaction logic for SphereGridNodeEditor.xaml
    /// </summary>
    public partial class NodeEditor : UserControl
    {
        public NodeEditor()
        {
            InitializeComponent();

            ListNodes.SelectedIndex = 0;
        }

        private void ListNodes_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            GridEditor.DataContext = FFXKernel.Nodes[ListNodes.SelectedIndex];
        }

        private void ButtonAbilityLearned_OnClick(object sender, RoutedEventArgs e)
        {
            var win = new LookupWindow(new List<LookupList>
            {
                new LookupList
                {
                    Title = "Commands",
                    List = FFXKernel.CommandDictionary.ToDictionary(c => (int) c.Key, c => c.Value)
                }
            }, ((Panel) GridEditor.DataContext).AbilityLearned);

            if (!win.ShowDialog() ?? true)
                return;

            ((Panel) GridEditor.DataContext).AbilityLearned = (ushort) win.UShortValue;
        }
    }
}