﻿using System.Windows.Controls;
using Omega.FFX;

namespace Omega.Panels.SphereGrid
{
    /// <summary>
    /// Interaction logic for NodeEditor.xaml
    /// </summary>
    public partial class SphereEditor : UserControl
    {
        public SphereEditor()
        {
            InitializeComponent();

            ListSpheres.SelectedIndex = 0;
        }

        private void ListSpheres_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            GridEditor.DataContext = FFXKernel.Spheres[ListSpheres.SelectedIndex];
        }
    }
}
