﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using FFXLib.FileSystem.Data.Records.FFX.Kernel;
using Omega.FFX;
using Omega.Windows;

namespace Omega.Panels.Kernel
{
    /// <summary>
    /// Interaction logic for ItemEditor.xaml
    /// </summary>
    public partial class ItemEditor : UserControl
    {
        private Item _selectedItem;

        public ItemEditor()
        {
            InitializeComponent();

            ListItems.SelectedIndex = 0;
        }

        private void ListItems_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ListItems.SelectedIndex == -1)
                return;

            GridEditor.DataContext = _selectedItem = (Item)ListItems.SelectedItem;
        }

        private void TextFilter_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            ListItems.ItemsSource = TextFilter.Text == string.Empty
                ? FFXKernel.Items //            Unfiltered list
                : FFXKernel.Items.Where(i => // Filter by name
                    i.Name.IndexOf(TextFilter.Text, StringComparison.CurrentCultureIgnoreCase) != -1);

            ListItems.SelectedItem = _selectedItem;
        }

        private void TextFilter_OnKeyDown(object sender, KeyEventArgs e)
        {
            // Clear filter when Escape is pressed
            if (e.Key == Key.Escape)
                TextFilter.Text = string.Empty;
        }

        private void ButtonCasterAnimation_OnClick(object sender, RoutedEventArgs e)
        {
            var win = new LookupWindow(new List<LookupList>
            {
                new LookupList
                {
                    Title = "Caster Animation",
                    List = FFXStrings.CasterAnims.ToDictionary(a => (int) a.Key, a => a.Value)
                }
            }, ((Item)GridEditor.DataContext).CasterAnimation);

            if (!win.ShowDialog() ?? true)
                return;

            ((Item)GridEditor.DataContext).CasterAnimation = (byte)win.UShortValue;
        }

        private void ButtonSpellAnimation_OnClick(object sender, RoutedEventArgs e)
        {
            if (!(sender is Button button))
                return;

            var binding = button.GetBindingExpression(ContentProperty);
            if (binding == null)
                return;

            var bindingProperty = binding.DataItem.GetType().GetProperty(binding.ResolvedSourcePropertyName);
            if (bindingProperty == null)
                return;

            var bindingValue = (ushort)bindingProperty.GetValue(binding.DataItem);
            var win = new LookupWindow(new List<LookupList>
            {
                new LookupList
                {
                    Title = "Spell Animation",
                    List = FFXStrings.SpellAnimations.ToDictionary(a => (int) a.Key, a => a.Value)
                },
            }, bindingValue);


            if (!win.ShowDialog() ?? true)
                return;

            bindingProperty.SetValue(binding.DataItem, (ushort)win.UShortValue);
        }
    }
}