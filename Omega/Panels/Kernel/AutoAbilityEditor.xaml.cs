﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using FFXLib.FileSystem.Data.Records.FFX.Kernel;
using Microsoft.WindowsAPICodePack.Dialogs;
using Newtonsoft.Json;
using Omega.FFX;
using Omega.Windows;

namespace Omega.Panels.Kernel
{
    /// <summary>
    /// Interaction logic for ItemEditor.xaml
    /// </summary>
    public partial class AutoAbilityEditor : UserControl
    {
        private AutoAbility _selectedItem;

        public AutoAbilityEditor()
        {
            InitializeComponent();

            
            ListAutoAbility.SelectedIndex = 0;
        }

        private void ListMonMagic_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ListAutoAbility.SelectedIndex == -1)
                return;

            GridEditor.DataContext = _selectedItem = (AutoAbility) ListAutoAbility.SelectedItem;
        }

        private void TextFilter_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            ListAutoAbility.ItemsSource = TextFilter.Text == string.Empty
                ? FFXKernel.AutoAbility //            Unfiltered list
                : FFXKernel.AutoAbility.Where(i => // Filter by name
                    i.Name.IndexOf(TextFilter.Text, StringComparison.CurrentCultureIgnoreCase) != -1);

            ListAutoAbility.SelectedItem = _selectedItem;
        }

        private void TextFilter_OnKeyDown(object sender, KeyEventArgs e)
        {
            // Clear filter when Escape is pressed
            if (e.Key == Key.Escape)
                TextFilter.Text = string.Empty;
        }

        private void ButtonExport_OnClick(object sender, RoutedEventArgs e)
        {
            var fileDialog = new CommonSaveFileDialog
            {
                Title = $"Export {_selectedItem.Name}",
                DefaultFileName = $"{_selectedItem.Name.ToLower().Replace(" ", "_")}.json",
                Filters =
                {
                    new CommonFileDialogFilter("Json files", "*.json")
                }
            };

            if (fileDialog.ShowDialog() != CommonFileDialogResult.Ok)
                return;

            var json = JsonConvert.SerializeObject(_selectedItem, Formatting.Indented);
            try
            {
                File.WriteAllText(fileDialog.FileName, json);
            }
            catch (Exception)
            {
                // TODO: MainWindow error message
            }
        }

        private void ButtonImport_Click(object sender, RoutedEventArgs e)
        {
            var fileDialog = new CommonOpenFileDialog
            {
                Title = $"Export {_selectedItem.Name}",
                Filters =
                {
                    new CommonFileDialogFilter("Json files", "*.json")
                }
            };

            if (fileDialog.ShowDialog() != CommonFileDialogResult.Ok)
                return;

            var json = File.ReadAllText(fileDialog.FileName);
            var command = JsonConvert.DeserializeObject<AutoAbility>(json);

            //FFXKernel.ReplaceCommand(_selectedItem, command);
        }
    }
}