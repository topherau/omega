﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using FFXLib.FileSystem.Data.Records.FFX.Kernel;
using Microsoft.WindowsAPICodePack.Dialogs;
using Newtonsoft.Json;
using Omega.FFX;
using Omega.Windows;

namespace Omega.Panels.Kernel
{
    /// <summary>
    /// Interaction logic for ItemEditor.xaml
    /// </summary>
    public partial class CustomizationEditor : UserControl
    {
        private Kaizou _selectedItem;

        public CustomizationEditor()
        {
            InitializeComponent();

            ListCustomization.SelectedIndex = 0;
        }

        private void ListCustomization_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ListCustomization.SelectedIndex == -1)
                return;

            GridEditor.DataContext = _selectedItem = FFXKernel.Customization[(ushort) ListCustomization.SelectedValue];
        }

        private void TextFilter_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            ListCustomization.ItemsSource = TextFilter.Text == string.Empty
                ? FFXKernel.CustomizationDictionary
                : FFXKernel.CustomizationDictionary
                    .Where(d => d.Value.IndexOf(TextFilter.Text, StringComparison.CurrentCultureIgnoreCase) != -1)
                    .ToDictionary(d => d.Key, d => d.Value);

            ListCustomization.SelectedItem = _selectedItem;
        }

        private void TextFilter_OnKeyDown(object sender, KeyEventArgs e)
        {
            // Clear filter when Escape is pressed
            if (e.Key == Key.Escape)
                TextFilter.Text = string.Empty;
        }

        private void ButtonExport_OnClick(object sender, RoutedEventArgs e)
        {
            string defaultName;
            if (_selectedItem.AutoAbilityId != 0 &&
                FFXKernel.AutoAbilityDictionary.ContainsKey(_selectedItem.AutoAbilityId))
            {
                defaultName = FFXKernel.AutoAbilityDictionary[_selectedItem.AutoAbilityId];
            }
            else
            {
                defaultName = $"customization_{FFXKernel.Customization.IndexOf(_selectedItem)}";
            }

            var fileDialog = new CommonSaveFileDialog
            {
                Title = $"Export auto ability",
                DefaultFileName = $"{defaultName.ToLower().Replace(" ", "_")}.json",
                Filters =
                {
                    new CommonFileDialogFilter("Json files", "*.json")
                }
            };

            if (fileDialog.ShowDialog() != CommonFileDialogResult.Ok)
                return;

            var json = JsonConvert.SerializeObject(_selectedItem, Formatting.Indented);
            try
            {
                File.WriteAllText(fileDialog.FileName, json);
            }
            catch (Exception)
            {
                // TODO: MainWindow error message
            }
        }

        private void ButtonImport_Click(object sender, RoutedEventArgs e)
        {
            var fileDialog = new CommonOpenFileDialog
            {
                Title = "Import auto ability",
                Filters =
                {
                    new CommonFileDialogFilter("Json files", "*.json")
                }
            };

            if (fileDialog.ShowDialog() != CommonFileDialogResult.Ok)
                return;

            var json = File.ReadAllText(fileDialog.FileName);
            var command = JsonConvert.DeserializeObject<AutoAbility>(json);

            //FFXKernel.ReplaceCommand(_selectedItem, command);
        }

        private void ButtonItem_OnClick(object sender, RoutedEventArgs e)
        {
            if (!(e.OriginalSource is Button button))
                return;

            var binding = button.GetBindingExpression(ContentProperty);
            if (binding == null)
                return;

            var property = _selectedItem.GetType().GetProperty(binding.ResolvedSourcePropertyName);
            if (property == null)
                return;

            var value = property.GetValue(_selectedItem);

            var window = new LookupWindow(new List<LookupList>
            {
                new LookupList
                {
                    List = FFXKernel.ItemDictionary.ToDictionary(i => (int) i.Key, i => i.Value)
                }
            }, Convert.ToInt32(value));

            if (!window.ShowDialog() ?? true)
                return;

            property.SetValue(_selectedItem, (ushort) window.UShortValue);
        }

        private void ButtonAutoAbility_OnClick(object sender, RoutedEventArgs e)
        {
            if (!(e.OriginalSource is Button button))
                return;

            var binding = button.GetBindingExpression(ContentProperty);
            if (binding == null)
                return;

            var property = _selectedItem.GetType().GetProperty(binding.ResolvedSourcePropertyName);
            if (property == null)
                return;

            var value = property.GetValue(_selectedItem);

            var window = new LookupWindow(new List<LookupList>
            {
                new LookupList
                {
                    List = FFXKernel.AutoAbilityDictionary.ToDictionary(i => (int) i.Key, i => i.Value)
                }
            }, Convert.ToInt32(value));

            if (!window.ShowDialog() ?? true)
                return;

            var selectedValue = ListCustomization.SelectedValue;

            property.SetValue(_selectedItem, (ushort) window.UShortValue);

            ListCustomization.SelectedValue = selectedValue;
        }
    }
}