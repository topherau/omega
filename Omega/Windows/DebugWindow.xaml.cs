﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls.Dialogs;
using Omega.FFX;

namespace Omega.Windows
{
    /// <summary>
    /// Interaction logic for DebugWindow.xaml
    /// </summary>
    public partial class DebugWindow
    {
        public DebugWindow()
        {
            InitializeComponent();
        }

        private void ProcessSelectBox_OnDropDownOpened(object sender, EventArgs e)
        {
            // Refresh available processes
            ProcessSelectBox.ItemsSource = Process.GetProcesses()
                .OrderByDescending(p => p.ProcessName.IndexOf("FFX", StringComparison.CurrentCulture) != -1)
                .ThenByDescending(p => p.Id);
        }

        private async void AttachButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (ProcessSelectBox.SelectedItem == null)
                return;

            try
            {
                var process = (Process) ProcessSelectBox.SelectedItem;
                if (process.HasExited)
                    return;

                if (!FFXDebug.ValidateProcess(process))
                {
                    var answer = await this.ShowMessageAsync("Unknown process",
                        "The selected process doesn't appear to be Final Fantasy X.\n\n" +
                        "Attach anyway?", MessageDialogStyle.AffirmativeAndNegative, new MetroDialogSettings
                        {
                            AffirmativeButtonText = "Yes",
                            NegativeButtonText = "No"
                        });
                    if (answer == MessageDialogResult.Negative)
                        return;
                }

                if (FFXDebug.Attach(process))
                    return;
            }
            catch (Exception)
            {
                // TODO: Log
            }

            await this.ShowMessageAsync("Attach failed",
                "Something went wrong while attempting to attach to the process.");
        }

        private void ButtonTriggerBattle_OnClick(object sender, RoutedEventArgs e)
        {
            if (!ushort.TryParse(MapIdBox.Text, out var mapId))
                return;

            if (!byte.TryParse(BattleIdBox.Text, out var battleId))
                return;

            FFXDebug.TriggerBattle(mapId, battleId);
        }

        private void WriteItemButton_OnClick(object sender, RoutedEventArgs e)
        {
            FFXDebug.WriteItems();
        }

        private void WriteCommandButton_OnClick(object sender, RoutedEventArgs e)
        {
            FFXDebug.WriteCommands();
        }

        private void WriteAutoAbilityButton_OnClick(object sender, RoutedEventArgs e)
        {
            FFXDebug.WriteAutoAbilities();
        }

        private void WriteCustomizationButton_OnClick(object sender, RoutedEventArgs e)
        {
            FFXDebug.WriteCustomizations();
        }
    }
}