﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Omega.Windows
{
    /// <summary>
    /// Interaction logic for FlagsWindow.xaml
    /// </summary>
    public partial class FlagsWindow
    {
        public object ReturnValue { get; private set; }

        private Type _valueType;

        public FlagsWindow(Window owner, object flagsValue, string propertyName)
        {
            Owner = owner;
            InitializeComponent();

            _valueType = flagsValue.GetType();
            var valueLength = Marshal.SizeOf(flagsValue.GetType()) * 8;
            var longValue = (ulong)Convert.ChangeType(flagsValue, typeof(ulong));
            for (var i = valueLength - 1; i >= 0; i--)
            {
                var flagCheckBox = new CheckBox
                {
                    Content = i.ToString(),
                    Margin = new Thickness(2),
                    IsChecked = (longValue & (1UL << i)) != 0
                };
                FlagsGrid.Children.Add(flagCheckBox);
            }

            FlagsGroup.Header = $"Flags for {propertyName}";
        }

        private void ButtonSave_OnClick(object sender, RoutedEventArgs e)
        {
            var returnValue = 0UL;

            var numFlags = FlagsGrid.Children.Count;
            for (var i = 0; i < numFlags; i++)
            {
                var flagCheckBox = (CheckBox)FlagsGrid.Children[i];
                if (flagCheckBox.IsChecked ?? false)
                    returnValue |= 1UL << (numFlags - i - 1);
            }

            ReturnValue = Convert.ChangeType(returnValue, _valueType);
            DialogResult = true;
            Close();
        }
    }
}