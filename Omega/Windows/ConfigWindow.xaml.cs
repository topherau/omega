﻿using System.Windows;
using System.Windows.Controls;
using MahApps.Metro;
using Microsoft.WindowsAPICodePack.Dialogs;

namespace Omega.Windows
{
    /// <summary>
    /// Interaction logic for ConfigWindow.xaml
    /// </summary>
    public partial class ConfigWindow
    {
        public ConfigWindow()
        {
            InitializeComponent();
        }

        private void ButtonLocate_OnClick(object sender, RoutedEventArgs e)
        {
            var finderDialog = new CommonOpenFileDialog
            {
                Title = "Locate game installation",
                IsFolderPicker = true
            };
            var result = finderDialog.ShowDialog();
            if (result != CommonFileDialogResult.Ok)
                return;

            App.Config.GameDirectory = finderDialog.FileName;
        }

        private void ThemeSelector_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ThemeManager.ChangeAppStyle(Application.Current,
                ThemeManager.GetAccent(App.Config.Accent),
                ThemeManager.GetAppTheme(App.Config.Theme));
        }
    }
}