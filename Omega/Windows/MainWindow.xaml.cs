﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using MahApps.Metro.Controls.Dialogs;
using Microsoft.WindowsAPICodePack.Dialogs;
using Omega.FFX;
using Omega.FFX.SphereGrid;
using Omega.Panels;
using Omega.Panels.Battle;
using Omega.Panels.Kernel;
using Omega.Panels.SphereGrid;

namespace Omega.Windows
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : INotifyPropertyChanged
    {
        public bool IsFolderOpen { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        private readonly Dictionary<string, FrameworkElement> _editors = new Dictionary<string, FrameworkElement>();
        private SphereGridWindow _gridStandard, _gridExpert;
        private DebugWindow _debugWindow;
        private UserControl _currentEditor;

        public MainWindow()
        {
            InitializeComponent();

            if (App.Config.OpenAutomatically && App.Config.GameDirectory != null &&
                Directory.Exists(App.Config.GameDirectory))
            {
                Show();
                LoadWorkDirectory(true);
            }
        }

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void CreateEditor<TEditor>() where TEditor : FrameworkElement
        {
            var editor = Activator.CreateInstance<TEditor>();
            editor.Visibility = Visibility.Collapsed;
            ContentGrid.Children.Add(editor);
            _editors[typeof(TEditor).Name] = editor;
        }

        private async void LoadWorkDirectory(bool failSilently)
        {
            var controller = await this.ShowProgressAsync("Loading...", "Please wait while Omega reads the game data and initializes the editor...", false, new MetroDialogSettings
            {
                AnimateHide = App.Config.EnableAnimations,
                AnimateShow = App.Config.EnableAnimations
            });
            controller.SetIndeterminate();

            try
            {
                var result = await Task.Run(() =>
                {
                    FFXData.Load();

                    IsFolderOpen = true;
                    return true;
                });

                await controller.CloseAsync();
                if (result)
                {
                    // Load successful

                    CreateEditor<ItemEditor>();
                    CreateEditor<CommandEditor>();
                    CreateEditor<MonMagicEditor>();
                    CreateEditor<AutoAbilityEditor>();
                    CreateEditor<CustomizationEditor>();
                    CreateEditor<MonsterEditor>();
                    CreateEditor<NodeEditor>();
                    CreateEditor<SphereEditor>();
                }
                else
                {
                    // Load failed

                    if (failSilently)
                        return;

                    await this.ShowMessageAsync("Error",
                        "An error occurred while attempting to open the work directory.", MessageDialogStyle.Affirmative, new MetroDialogSettings
                        {
                            AnimateHide = App.Config.EnableAnimations,
                            AnimateShow = App.Config.EnableAnimations
                        });
                }
            }
            catch (FileNotFoundException)
            {
                // Failed to locate game files
                await controller.CloseAsync();

                if (failSilently)
                    return;

                await this.ShowMessageAsync("Game data missing",
                    "Omega was unable to locate the necessary game files. Please ensure the " +
                    "correct game directory is selected in the configuration.", MessageDialogStyle.Affirmative, new MetroDialogSettings
                    {
                        AnimateHide = App.Config.EnableAnimations,
                        AnimateShow = App.Config.EnableAnimations
                    });
                new ConfigWindow {Owner = this}.ShowDialog();
            }
            catch (Exception ex)
            {
                // General exception
                await controller.CloseAsync();

                if (failSilently)
                    return;

                await this.ShowMessageAsync("Exception occurred",
                    $"An exception occurred while attempting to load the game data:\n\n{ex.Message}", MessageDialogStyle.Affirmative, new MetroDialogSettings
                    {
                        AnimateHide = App.Config.EnableAnimations,
                        AnimateShow = App.Config.EnableAnimations
                    });
            }
        }

        private async Task<bool> ConfirmExit()
        {
            if (!FFXData.IsLoaded || !FFXData.IsDirty())
                return true;

            var result = await this.ShowMessageAsync("Confirm exit",
                "You have changes that have not yet been saved.\n\nAre you sure you want to exit?",
                MessageDialogStyle.AffirmativeAndNegative, new MetroDialogSettings
                {
                    AffirmativeButtonText = "Exit Omega",
                    NegativeButtonText = "Back to editor"
                });

            if (result != MessageDialogResult.Affirmative)
                return false;

            return true;
        }

        #region Menu: Main

        private void MenuMain_OpenWork_OnClick(object sender, RoutedEventArgs e)
        {
            var picker = new CommonOpenFileDialog
            {
                Title = "Select working directory",
                IsFolderPicker = true
            };

            if (picker.ShowDialog() != CommonFileDialogResult.Ok)
                return;

            App.Config.WorkDirectory = picker.FileName;

            LoadWorkDirectory(false);
        }

        private async void MenuMain_CloseWork_OnClick(object sender, RoutedEventArgs e)
        {
            _currentEditor = null;
            _editors.Clear();
            ContentGrid.Children.Clear();
            if (FFXData.Unload())
            {
                // Unload successful
                IsFolderOpen = false;
            }
            else
            {
                // Unload failed
                await this.ShowMessageAsync("Error",
                    "An error occurred while closing the work directory, and Omega will now exit.");
                Application.Current.Shutdown();
            }
        }

        private void MenuMain_Config_OnClick(object sender, RoutedEventArgs e)
        {
            new ConfigWindow {Owner = this}.ShowDialog();
        }

        private async void MenuMain_Exit_OnClick(object sender, RoutedEventArgs e)
        {
            if (await ConfirmExit())
                Application.Current.Shutdown();
        }

        #endregion

        #region Menu: Help

        private void MenuHelp_About_OnClick(object sender, RoutedEventArgs e)
        {
            new AboutWindow {Owner = this}.ShowDialog();
        }

        #endregion

        private void Menu_EditorMenu_OnClick(object sender, RoutedEventArgs e)
        {
            // First we get the name of the editor from the menu item's tag property
            if (!(sender is FrameworkElement element))
                return;

            if (!(element.Tag is string tag))
                return;
            
            if (!_editors.ContainsKey(tag))
                return;

            // Don't attempt to transition if new editor is same as current editor
            if (Equals(_currentEditor, _editors[tag]))
                return;

            // Pre-transition preparation
            if (App.Config.EnableAnimations)
            {
                // Create a new RenderTargetBitmap to draw the current editor onto
                var renderTargetBitmap = new RenderTargetBitmap((int) ContentGrid.ActualWidth,
                    (int) ContentGrid.ActualHeight, 96, 96,
                    PixelFormats.Pbgra32);

                // Create a visual to fill in the transparent background
                var backgroundBrush = (Brush) Application.Current.Resources["ControlBackgroundBrush"];
                var backgroundVisual = new DrawingVisual();
                using (var drawingContext = backgroundVisual.RenderOpen())
                    drawingContext.DrawRectangle(backgroundBrush, null,
                        new Rect(new Size(ContentGrid.ActualWidth, ContentGrid.ActualHeight)));

                // Render background color to bitmap
                renderTargetBitmap.Render(backgroundVisual);

                // Render the editor onto the bitmap, if it exists
                if (_currentEditor != null)
                    renderTargetBitmap.Render(_currentEditor);

                // Show the overlay of the old editor while we load in the new
                ContentGrid.Background = backgroundBrush;
                ImageOverlay.Source = renderTargetBitmap;
            }
            
            // Change visibility of inactive editors to Hidden
            foreach (var editor in _editors)
            {
                if (editor.Key == tag)
                {
                    editor.Value.Visibility = Visibility.Visible;
                }
                else if (editor.Value.Visibility == Visibility.Visible)
                {
                    editor.Value.Visibility = Visibility.Hidden;
                }
            }

            // Force layout update before animation
            ContentGrid.UpdateLayout();

            // Post-transition animation
            if (App.Config.EnableAnimations)
            {
                // Change editor content opacity to zero (fade in later)
                ContentGrid.Opacity = 0;

                // Once the editor is loaded and the layout is complete, hide the overlay with a pretty animation
                var animationTime = TimeSpan.FromMilliseconds(350);

                var overlayOpacityAnimation = new DoubleAnimation(1, 0, animationTime);
                var contentOpacityAnimation = new DoubleAnimation(0, 1, animationTime);
                var scaleXAnimation =
                    new DoubleAnimation(ContentGrid.ActualWidth, ContentGrid.ActualWidth * 0.9, animationTime);
                var scaleYAnimation =
                    new DoubleAnimation(ContentGrid.ActualHeight, ContentGrid.ActualHeight * 0.9, animationTime);

                Storyboard.SetTarget(overlayOpacityAnimation, ImageOverlay);
                Storyboard.SetTarget(contentOpacityAnimation, ContentGrid);
                overlayOpacityAnimation.Completed += (o, args) => { ContentGrid.Background = null; };
                Storyboard.SetTarget(scaleXAnimation, ImageOverlay);
                Storyboard.SetTarget(scaleYAnimation, ImageOverlay);

                Storyboard.SetTargetProperty(overlayOpacityAnimation, new PropertyPath(OpacityProperty));
                Storyboard.SetTargetProperty(contentOpacityAnimation, new PropertyPath(OpacityProperty));
                Storyboard.SetTargetProperty(scaleXAnimation, new PropertyPath(WidthProperty));
                Storyboard.SetTargetProperty(scaleYAnimation, new PropertyPath(HeightProperty));

                var storyboard = new Storyboard
                {
                    Children =
                {
                    overlayOpacityAnimation,
                    contentOpacityAnimation,
                    scaleXAnimation,
                    scaleYAnimation
                }
                };

                storyboard.Begin(ImageOverlay, true);
            }
            
            _currentEditor = (UserControl)_editors[tag];
        }

        private void MenuSphereGrid_LayoutStandard_Click(object sender, RoutedEventArgs e)
        {
            if (_gridStandard != null)
            {
                _gridStandard.Focus();
                return;
            }

            _gridStandard = new SphereGridWindow(FFXSphereGrid.Standard);
            _gridStandard.Closed += (o, args) => { _gridStandard = null; };
            _gridStandard.Show();
        }

        private void MenuSphereGrid_LayoutExpert_Click(object sender, RoutedEventArgs e)
        {
            if (_gridExpert != null)
            {
                _gridExpert.Focus();
                return;
            }

            _gridExpert = new SphereGridWindow(FFXSphereGrid.Expert);
            _gridExpert.Closed += (o, args) => { _gridExpert = null; };
            _gridExpert.Show();
        }

        private void MenuMain_SaveData_OnClick(object sender, RoutedEventArgs e)
        {
            FFXKernel.Save();
        }

        private void MenuMain_Debug_OnClick(object sender, RoutedEventArgs e)
        {
            if (_debugWindow != null)
            {
                _debugWindow.Focus();
                return;
            }

            _debugWindow = new DebugWindow();
            _debugWindow.Closed += (o, args) => { _debugWindow = null; };
            _debugWindow.Show();
        }
    }
}