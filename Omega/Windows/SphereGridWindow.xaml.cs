﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using FFXLib;
using Omega.Classes.SphereGrid;
using Omega.FFX.SphereGrid;
using Omega.FFX.SphereGrid.Controls;
using Grid = Omega.Classes.SphereGrid.Grid;

namespace Omega.Windows
{
    /// <summary>
    /// Interaction logic for SphereGridWindow.xaml
    /// </summary>
    public partial class SphereGridWindow : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public string StatusText { get; set; }
        public double ZoomLevel { get; private set; } = 1.0;

        private double _dragOffsetH;
        private double _dragOffsetV;
        private Point _dragStart;
        private bool _dragStarting;
        private Storyboard _zoomStoryboard;

        public SphereGridWindow(Grid grid)
        {
            InitializeComponent();

            SphereGrid.LoadGrid(grid);
            SphereGrid.ObjectFocused += SphereGrid_OnObjectFocused;

            Title = $"{(grid.Type == GridType.Standard ? "Standard" : "Expert")} Sphere Grid - Omega";
            SphereGrid.PropertyChanged += SphereGrid_OnPropertyChanged;
            UpdateStatusText();
        }

        private void SphereGrid_OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "SelectedObject":
                case "InputMode":
                    UpdateStatusText();
                    break;
            }
        }

        private void UpdateStatusText()
        {
            switch (SphereGrid.InputMode)
            {
                case GridInputMode.Default:
                    if (SphereGrid.SelectedObject != null)
                    {
                        if (SphereGrid.SelectedObject is GridGlyph glyph)
                        {
                            StatusText = $"Currently editing glyph {glyph.Id}.";
                        }
                        else if (SphereGrid.SelectedObject is GridNode node)
                        {
                            StatusText = $"Currently editing node {node.Id}.";
                        }
                        else if (SphereGrid.SelectedObject is GridLine line)
                        {
                            StatusText = $"Currently editing line {line.Id}.";
                        }
                    }
                    else
                    {
                        StatusText = "Double click a node, line or glyph to edit its' properties.";
                    }

                    break;
                case GridInputMode.CreateLine:
                {
                    if (SphereGrid.SelectedObject is GridNode node)
                        StatusText = "Select a destination node for the link (right-click to cancel).";
                    break;
                    }
                case GridInputMode.Dragging:
                {
                    StatusText = "Dragging objects (right-click to cancel).";
                        break;
                }
                case GridInputMode.SetCenter:
                {
                    StatusText = "Select a new center point (right-click to cancel).";
                    break;
                }
            }
        }

        private void SphereGrid_OnObjectFocused(object focusedObject)
        {
            if (focusedObject is GridNode node)
            {
                NodePanel.DataContext = node;
                GlyphPanel.Visibility = Visibility.Collapsed;
                NodePanel.Visibility = Visibility.Visible;
            }
            else if (focusedObject is GridGlyph glyph)
            {
                GlyphPanel.DataContext = glyph;
                GlyphPanel.Visibility = Visibility.Visible;
                NodePanel.Visibility = Visibility.Collapsed;
            } else if (focusedObject is GridLine line)
            {
                // TODO: Line panel
            }
            else
            {
                GlyphPanel.Visibility = Visibility.Collapsed;
                NodePanel.Visibility = Visibility.Collapsed;
            }
        }

        private void ButtonSettings_OnClick(object sender, RoutedEventArgs e)
        {
            SettingsFlyout.IsOpen = true;
        }

        #region Dependency properties

        public static DependencyProperty HorizontalOffsetProperty =
            DependencyProperty.RegisterAttached("HorizontalOffset", typeof(double),
                typeof(SphereGridWindow), new FrameworkPropertyMetadata(0.0, OnHorizontalOffsetChanged));

        public static DependencyProperty VerticalOffsetProperty =
            DependencyProperty.RegisterAttached("VerticalOffset", typeof(double),
                typeof(SphereGridWindow), new FrameworkPropertyMetadata(0.0, OnVerticalOffsetChanged));

        private static void OnVerticalOffsetChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var sv = (SphereGridWindow) d;
            sv.GridScroll.ScrollToVerticalOffset((double) e.NewValue);
        }

        private static void OnHorizontalOffsetChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var sv = (SphereGridWindow) d;
            sv.GridScroll.ScrollToHorizontalOffset((double) e.NewValue);
        }

        #endregion

        #region ScrollViewer mouse input

        private void GridScroll_OnPreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            _zoomStoryboard?.Pause(this);
            _dragOffsetH = GridScroll.HorizontalOffset;
            _dragOffsetV = GridScroll.VerticalOffset;
            _dragStart = e.GetPosition(GridScroll);
            _dragStarting = true;
        }

        private void GridScroll_OnPreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (_dragStarting)
                _dragStarting = false;

            if (!GridScroll.IsMouseCaptured)
                return;

            GridScroll.ReleaseMouseCapture();
        }

        private void GridScroll_OnPreviewMouseMove(object sender, MouseEventArgs e)
        {
            if (_dragStarting && SphereGrid.IsDragging)
            {
                _dragStarting = false;
                return;
            }

            if (!_dragStarting && !GridScroll.IsMouseCaptured)
            {
                return;
            }

            var mousePos = e.GetPosition(GridScroll);
            if (_dragStarting)
            {
                // Test if mouse has moved far enough to start dragging
                var moveDistance =
                    Math.Sqrt(Math.Pow(mousePos.X - _dragStart.X, 2) + Math.Pow(mousePos.Y - _dragStart.Y, 2));
                if (moveDistance < 5)
                    return;

                // Start dragging
                GridScroll.CaptureMouse();
                _dragStarting = false;
            }

            GridScroll.ScrollToHorizontalOffset(_dragOffsetH + (_dragStart.X - mousePos.X));
            GridScroll.ScrollToVerticalOffset(_dragOffsetV + (_dragStart.Y - mousePos.Y));
        }

        private void GridScroll_OnPreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            e.Handled = true;
            var moveVal = 0.25;

            var newZoomLevel = ZoomLevel + (moveVal * (e.Delta < 0 ? -1 : 1));

            if (newZoomLevel < 0.25)
                newZoomLevel = 0.25;
            else if (newZoomLevel > 3)
                newZoomLevel = 3;

            // ReSharper disable once CompareOfFloatsByEqualityOperator
            if (newZoomLevel == ZoomLevel)
                return;

            ZoomLevel = newZoomLevel;
            ZoomTo(ZoomLevel, Mouse.GetPosition(SphereGrid));
        }

        #endregion

        private void ZoomTo(double level, Point centerPoint)
        {
            if (GridScroll.ActualWidth == 0)
                return;

            if (GridScroll.IsMouseCaptured)
                return;

            // Stop existing storyboard
            _zoomStoryboard?.Stop();
            _zoomStoryboard?.Remove();

            var animationTime = TimeSpan.FromMilliseconds(500);

            var transform = SphereGrid.LayoutTransform as ScaleTransform;

            // Animate scale change
            var scaleAnimationX = new DoubleAnimation(transform.ScaleX, level, animationTime);
            var scaleAnimationY = new DoubleAnimation(transform.ScaleY, level, animationTime);

            Storyboard.SetTarget(scaleAnimationX, SphereGrid);
            Storyboard.SetTarget(scaleAnimationY, SphereGrid);
            Storyboard.SetTargetProperty(scaleAnimationX, new PropertyPath("LayoutTransform.(ScaleTransform.ScaleX)"));
            Storyboard.SetTargetProperty(scaleAnimationY, new PropertyPath("LayoutTransform.(ScaleTransform.ScaleY)"));

            // Animate scroll to new center point
            var scrollOffsetH = centerPoint.X / SphereGrid.ActualWidth * (SphereGrid.ActualWidth * level) -
                                GridScroll.ActualWidth / 2;
            var scrollOffsetV = centerPoint.Y / SphereGrid.ActualHeight * (SphereGrid.ActualHeight * level) -
                                GridScroll.ActualHeight / 2;
            var scrollAnimationH = new DoubleAnimation(GridScroll.HorizontalOffset, scrollOffsetH,
                animationTime);
            var scrollAnimationV = new DoubleAnimation(GridScroll.VerticalOffset, scrollOffsetV,
                animationTime);

            Storyboard.SetTarget(scrollAnimationH, this);
            Storyboard.SetTarget(scrollAnimationV, this);
            Storyboard.SetTargetProperty(scrollAnimationH, new PropertyPath("HorizontalOffset"));
            Storyboard.SetTargetProperty(scrollAnimationV, new PropertyPath("VerticalOffset"));

            _zoomStoryboard = new Storyboard
            {
                Children =
                {
                    scaleAnimationX,
                    scaleAnimationY,
                    scrollAnimationH,
                    scrollAnimationV
                }
            };

            _zoomStoryboard.Begin(this, true);
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}