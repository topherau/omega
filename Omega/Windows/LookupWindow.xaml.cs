﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using FFXLib.Extension;
using MahApps.Metro.Controls;
using Xceed.Wpf.Toolkit.Core.Utilities;

namespace Omega.Windows
{
    /// <summary>
    /// Interaction logic for LookupWindow.xaml
    /// </summary>
    public partial class LookupWindow
    {
        public uint UIntValue => Convert.ToUInt32(_selectedValue);
        public ushort UShortValue => Convert.ToUInt16(_selectedValue);
        public byte ByteValue => Convert.ToByte(_selectedValue);

        private LookupList[] _lists;
        private int _selectedValue;

        public LookupWindow(IEnumerable<LookupList> lists, int defaultValue)
        {
            InitializeComponent();
            Owner = Application.Current.MainWindow;
            _lists = lists.ToArray();
            if (_lists.Length == 1)
                TabLookupType.Visibility = Visibility.Collapsed;

            // Add all lists to tab control
            foreach (var list in _lists)
            {
                var item = new TabItem {Header = list.Title};
                ControlsHelper.SetHeaderFontSize(item, 16);
                TabLookupType.Items.Add(item);
                if (TabLookupType.SelectedIndex == -1 && list.List.ContainsKey(defaultValue))
                {
                    _selectedValue = defaultValue;
                    TabLookupType.SelectedItem = item;
                    if (ListLookup.SelectedItem != null)
                    {
                        ListLookup.ScrollIntoView(ListLookup.SelectedItem);
                    }
                }
            }
        }

        private void TabLookupType_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ListLookup.ItemsSource = _lists[TabLookupType.SelectedIndex].List;
            ListLookup.SelectedValue = _selectedValue;
        }

        private void ButtonConfirm_OnClick(object sender, RoutedEventArgs e)
        {
            _selectedValue = (int) ListLookup.SelectedValue;
            DialogResult = true;
            Close();
        }

        private void ListLookup_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ListLookup.SelectedValue == null)
                return;

            _selectedValue = (int)ListLookup.SelectedValue;
        }
    }

    public class LookupList
    {
        public string Title { get; set; }
        public Dictionary<int, string> List { get; set; }
    }
}