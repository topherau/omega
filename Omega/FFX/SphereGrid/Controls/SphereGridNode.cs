﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using FFXLib;
using Omega.Classes.SphereGrid;

namespace Omega.FFX.SphereGrid.Controls
{
    public class SphereGridNode : Image, INotifyPropertyChanged
    {
        public GridNode Node { get; }
        public SphereGrid Grid { get; }

        public Color HighlightColor
        {
            get => (Color) GetValue(HighlightColorProperty);
            set
            {
                SetValue(HighlightColorProperty, value);
                OnPropertyChanged(nameof(HighlightColor));
                DropShadow.Color = value;
            }
        }

        public bool IsHighlighted
        {
            get => (bool) GetValue(IsHighlightedProperty);
            set
            {
                SetValue(IsHighlightedProperty, value);
                OnPropertyChanged(nameof(IsHighlighted));
            }
        }

        public bool IsEnlarged
        {
            get => (bool) GetValue(IsEnlargedProperty);
            set
            {
                SetValue(IsEnlargedProperty, value);
                OnPropertyChanged(nameof(IsEnlarged));
            }
        }

        public bool IsActive
        {
            get => (bool) GetValue(IsActiveProperty);
            set
            {
                SetValue(IsActiveProperty, value);
                OnPropertyChanged(nameof(IsActive));
            }
        }


        public static readonly DependencyProperty IsEnlargedProperty = DependencyProperty.Register("IsEnlarged",
            typeof(bool), typeof(SphereGridNode),
            new FrameworkPropertyMetadata(false, OnIsEnlargedChanged));

        public static readonly DependencyProperty IsHighlightedProperty = DependencyProperty.Register("IsHighlighted",
            typeof(bool), typeof(SphereGridNode),
            new FrameworkPropertyMetadata(false, OnIsHighlightedChanged));

        public static readonly DependencyProperty IsActiveProperty = DependencyProperty.Register("IsActive",
            typeof(bool), typeof(SphereGridNode),
            new FrameworkPropertyMetadata(false, OnIsActiveChanged));

        public static readonly DependencyProperty HighlightColorProperty = DependencyProperty.Register("HighlightColor",
            typeof(Color), typeof(SphereGridNode),
            new FrameworkPropertyMetadata(Colors.Transparent, OnHighlightColorChanged));

        public DropShadowEffect DropShadow { get; } = new DropShadowEffect
        {
            BlurRadius = 20,
            ShadowDepth = 0
        };

        public ImageSource IconActive { get; set; }
        public ImageSource IconInactive { get; set; }

        public SphereGridNode(GridNode node, SphereGrid grid)
        {
            Node = node;
            Grid = grid;

            Style =
                new Style
                {
                    TargetType = typeof(SphereGridNode),
                    Setters =
                    {
                        new Setter(MarginProperty, new Thickness(-1000000)),
                        new Setter(SourceProperty, new Binding
                            {
                                Source = this,
                                Path = new PropertyPath("IconInactive")
                            }
                        )
                    },
                    Triggers =
                    {
                        new Trigger
                        {
                            Property = IsActiveProperty,
                            Value = true,
                            Setters =
                            {
                                new Setter
                                {
                                    Property = SourceProperty,
                                    Value = new Binding
                                    {
                                        Source = this,
                                        Path = new PropertyPath("IconActive")
                                    }
                                }
                            }
                        },
                        new Trigger
                        {
                            Property = IsHighlightedProperty,
                            Value = true,
                            Setters =
                            {
                                new Setter
                                {
                                    Property = EffectProperty,
                                    Value = new Binding
                                    {
                                        Source = this,
                                        Path = new PropertyPath("DropShadow")
                                    }
                                }
                            }
                        },
                        new Trigger
                        {
                            Property = IsEnlargedProperty,
                            Value = true,
                            Setters =
                            {
                                new Setter(LayoutTransformProperty, new ScaleTransform(1.25, 1.25))
                            }
                        },
                        new Trigger
                        {
                            Property = IsMouseOverProperty,
                            Value = true,
                            Setters =
                            {
                                new Setter(IsActiveProperty, true),
                                new Setter(IsHighlightedProperty, true),
                                new Setter(IsEnlargedProperty, true)
                            }
                        },
                        new DataTrigger
                        {
                            Binding = new Binding
                            {
                                Source = Grid,
                                Path = new PropertyPath("SelectedObject")
                            },
                            Value = Node,
                            Setters =
                            {
                                new Setter(IsActiveProperty, true),
                                new Setter(IsEnlargedProperty, true),
                                new Setter(EffectProperty,
                                    new DropShadowEffect
                                    {
                                        Color = Colors.Gold,
                                        ShadowDepth = 0,
                                        BlurRadius = 30
                                    })
                            }
                        }
                    }
                };


            node.PropertyChanged += (sender, args) =>
            {
                if (args.PropertyName != "NodeType")
                    return;

                UpdateImage();
            };

            Canvas.SetLeft(this, node.X);
            Canvas.SetTop(this, node.Y);

            UpdateImage();
        }

        public static void OnHighlightColorChanged(DependencyObject o, DependencyPropertyChangedEventArgs a)
        {
            ((SphereGridNode) o).HighlightColor = (Color) a.NewValue;
        }

        public static void OnIsHighlightedChanged(DependencyObject o, DependencyPropertyChangedEventArgs a)
        {
            ((SphereGridNode) o).IsHighlighted = (bool) a.NewValue;
        }

        public static void OnIsEnlargedChanged(DependencyObject o, DependencyPropertyChangedEventArgs a)
        {
            ((SphereGridNode) o).IsEnlarged = (bool) a.NewValue;
        }

        private static void OnIsActiveChanged(DependencyObject o, DependencyPropertyChangedEventArgs a)
        {
            ((SphereGridNode) o).IsActive = (bool) a.NewValue;
        }

        private void UpdateImage()
        {
            var activeIcons = (CroppedBitmap[]) Application.Current.Resources["SphereGridIconsActive"];
            var inactiveIcons = (CroppedBitmap[]) Application.Current.Resources["SphereGridIconsInactive"];
            if (Node.NodeType >= 255)
            {
                IconActive = activeIcons[1];
                IconInactive = inactiveIcons[1];
                Opacity = 0.5;
                Visibility = Visibility.Collapsed;
            }
            else
            {
                var node = FFXKernel.Nodes[Node.NodeType];
                IconActive = activeIcons[node.Icon];
                IconInactive = inactiveIcons[node.Icon];
                Opacity = 1.0;
            }

            //SetBinding(SourceProperty, new Binding
            //{
            //    Source = this,
            //    Path = new PropertyPath("IconInactive")
            //});
            Width = IconInactive.Width * 1.5;
            Height = IconInactive.Height * 1.5;
            HighlightColor = GetDefaultColor();
        }

        public Color GetDefaultColor()
        {
            var color = GetAverageColor((CroppedBitmap) IconActive);
            color *= 3;
            color.A = 255;
            return color;
        }

        private Color GetAverageColor(BitmapSource bitmap)
        {
            if (bitmap == null)
                return Colors.White;

            var format = bitmap.Format;

            if (format != PixelFormats.Bgr24 &&
                format != PixelFormats.Bgr32 &&
                format != PixelFormats.Bgra32 &&
                format != PixelFormats.Pbgra32)
            {
                throw new InvalidOperationException("BitmapSource must have Bgr24, Bgr32, Bgra32 or Pbgra32 format");
            }

            var width = bitmap.PixelWidth;
            var height = bitmap.PixelHeight;
            var numPixels = width * height;
            var bytesPerPixel = format.BitsPerPixel / 8;
            var pixelBuffer = new byte[numPixels * bytesPerPixel];

            bitmap.CopyPixels(pixelBuffer, width * bytesPerPixel, 0);

            long blue = 0;
            long green = 0;
            long red = 0;

            for (int i = 0; i < pixelBuffer.Length; i += bytesPerPixel)
            {
                blue += pixelBuffer[i];
                green += pixelBuffer[i + 1];
                red += pixelBuffer[i + 2];
            }

            return Color.FromRgb((byte) (red / numPixels), (byte) (green / numPixels),
                (byte) (blue / numPixels));
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}