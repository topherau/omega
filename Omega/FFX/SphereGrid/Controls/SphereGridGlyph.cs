﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using FFXLib;
using Omega.Classes.SphereGrid;

namespace Omega.FFX.SphereGrid.Controls
{
    public class SphereGridGlyph : Image, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public GridGlyph Glyph { get; private set; }
        public SphereGrid Grid { get; }
        
        private CroppedBitmap[] _glyphBitmaps;

        private DropShadowEffect DropShadow { get; } = new DropShadowEffect
        {
            BlurRadius = 20,
            ShadowDepth = 0
        };

        #region Dependency properties

        public static DependencyProperty IsHighlightedProperty = DependencyProperty.Register("IsHighlighted",
            typeof(bool), typeof(SphereGridGlyph),
            new FrameworkPropertyMetadata(false, OnIsHighlightedChanged));

        public static DependencyProperty HighlightColorProperty = DependencyProperty.Register("HighlightColor",
            typeof(Color), typeof(SphereGridGlyph),
            new FrameworkPropertyMetadata(Colors.White, OnHighlightColorChanged));
        
        private static void OnIsHighlightedChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((SphereGridGlyph) d).IsHighlighted = (bool) e.NewValue;
        }

        private static void OnHighlightColorChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {

            ((SphereGridGlyph)d).HighlightColor = (Color) e.NewValue;
        }

        public bool IsHighlighted
        {
            get => (bool)GetValue(IsHighlightedProperty);
            set => SetValue(IsHighlightedProperty, value);
        }

        public Color HighlightColor
        {
            get => (Color)GetValue(HighlightColorProperty);
            set => SetValue(HighlightColorProperty, value);
        }

        #endregion

        public SphereGridGlyph(GridGlyph glyph, SphereGrid grid)
        {
            Grid = grid;
            Glyph = glyph;

            // Add event to update glyph image on property change
            Glyph.PropertyChanged += (sender, args) =>
            {
                if (args.PropertyName != "Type")
                    return;

                UpdateImage();
            };

            // Add glyph style
            Style = new Style
            {
                Setters =
                {
                    new Setter(MarginProperty, new Thickness(-1000000))
                },
                Triggers =
                {
                    new Trigger
                    {
                        Property = IsHighlightedProperty,
                        Value = true,
                        Setters =
                        {
                            new Setter(EffectProperty, new Binding
                            {
                                Source = this,
                                Path = new PropertyPath("DropShadow")
                            })
                        }
                    },
                    new DataTrigger
                    {
                        Binding = new Binding
                        {
                            Source = Grid,
                            Path = new PropertyPath("SelectedObject")
                        },
                        Value = Glyph,
                        Setters =
                        {
                            new Setter(EffectProperty, new DropShadowEffect
                            {
                                Color = Colors.Gold,
                                ShadowDepth = 0,
                                BlurRadius = 30
                            })
                        }
                    }
                }
            };

            // Position glyph
            Canvas.SetLeft(this, Glyph.X);
            Canvas.SetTop(this, Glyph.Y);

            // Load appropriate glyph images for standard/expert
            _glyphBitmaps = (CroppedBitmap[])Application.Current.TryFindResource(glyph.Grid.Type == GridType.Standard
                ? "SphereGridGlyphsStandard"
                : "SphereGridGlyphsExpert");

            UpdateImage();
        }

        public void UpdateImage()
        {
            var image = _glyphBitmaps[Glyph.Type];
            var width = image.PixelWidth;
            var height = image.PixelHeight;

            if (Glyph.Type != 2)
            {
                width /= 2;
                height /= 2;
            }
            Source = image;
            Width = width;
            Height = height;
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}