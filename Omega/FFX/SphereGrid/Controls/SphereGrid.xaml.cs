﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using FFXLib;
using MahApps.Metro.Controls;
using Omega.Classes.SphereGrid;
using Grid = Omega.Classes.SphereGrid.Grid;

namespace Omega.FFX.SphereGrid.Controls
{
    /// <summary>
    /// Interaction logic for SphereGrid.xaml
    /// </summary>
    public partial class SphereGrid : INotifyPropertyChanged
    {
        public event ObjectFocusedHandler ObjectFocused;
        public event PropertyChangedEventHandler PropertyChanged;

        public delegate void ObjectFocusedHandler(object focusedObject);

        public object SelectedObject { get; set; }
        public GridInputMode InputMode { get; private set; }
        public bool Unlocked { get; set; }
        public bool IsDragging { get; private set; }

        private readonly uint _doubleClickTime = GetDoubleClickTime();
        private readonly Dictionary<GridNode, SphereGridNode> _nodes = new Dictionary<GridNode, SphereGridNode>();
        private readonly Dictionary<GridGlyph, SphereGridGlyph> _glyphs = new Dictionary<GridGlyph, SphereGridGlyph>();
        private readonly Dictionary<GridLine, SphereGridLine> _lines = new Dictionary<GridLine, SphereGridLine>();
        private Grid _grid;
        private object _clickedObject; //                   object for double click
        private DateTime _clickedTime = DateTime.Now; //    timer for double click
        private GridNode _startNode, _endNode; //           start and end nodes for path

        private Shape _newLine;

        private List<GridNode> _pathNodes, _pathHistory = new List<GridNode>();
        private List<GridLine> _pathLines = new List<GridLine>();
        private int _pathIndex;
        private int _pathLockLevel;

        [DllImport("user32.dll")]
        private static extern uint GetDoubleClickTime();

        public int PathLockLevel
        {
            get => _pathLockLevel;
            set
            {
                _pathLockLevel = value;
                _pathIndex = 0;
            }
        }

        public SphereGrid()
        {
            InitializeComponent();
        }

        public void LoadGrid(Grid grid)
        {
            _grid = grid;
            Background = ((ImageBrush[]) Application.Current.Resources["SphereGridBackgrounds"])[(int) grid.Type];

            foreach (var glyph in grid.Glyphs)
            {
                DrawGlyph(glyph);
            }

            foreach (var node in grid.Nodes)
            {
                DrawNode(node);
            }

            foreach (var line in grid.Lines)
            {
                DrawLine(line);
            }
        }

        #region Element drawing

        private void DrawGlyph(GridGlyph glyph)
        {
            if (_glyphs.ContainsKey(glyph))
            {
                // Remove existing glyph
                GlyphCanvas.Children.Remove(_glyphs[glyph]);
            }

            if (!_grid.Glyphs.Contains(glyph))
                return;

            _glyphs[glyph] = new SphereGridGlyph(glyph, this);
            GlyphCanvas.Children.Add(_glyphs[glyph]);
        }

        private void DrawNode(GridNode node)
        {
            if (_nodes.ContainsKey(node))
            {
                // Remove existing node
                NodeCanvas.Children.Remove(_nodes[node]);
            }

            if (!_grid.Nodes.Contains(node))
                return;

            _nodes[node] = new SphereGridNode(node, this);
            NodeCanvas.Children.Add(_nodes[node]);
        }

        private void DrawLine(GridLine line)
        {
            if (_lines.ContainsKey(line))
            {
                // Remove existing line
                LineCanvas.Children.Remove(_lines[line].LineShape);
            }

            if (!_grid.Lines.Contains(line))
                return;


            _lines[line] = new SphereGridLine(line, this);
            LineCanvas.Children.Add(_lines[line].LineShape);
        }

        #endregion

        #region Generic mouse input handlers

        private void SphereGrid_OnPreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            switch (InputMode)
            {
                case GridInputMode.Default:
                    Default_LeftButtonDown(e);
                    break;
                case GridInputMode.CreateLine:
                    CreateLine_LeftButtonDown(e);
                    break;
                case GridInputMode.SetCenter:
                    SetCenter_LeftButtonDown(e);
                    break;
            }
        }

        private void SphereGrid_OnPreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            switch (InputMode)
            {
                case GridInputMode.Default:
                    Default_LeftButtonUp(e);
                    break;
                case GridInputMode.Dragging:
                    Dragging_LeftButtonUp(e);
                    break;
            }
        }

        private void SphereGrid_OnPreviewMouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            switch (InputMode)
            {
                case GridInputMode.Default:
                    Default_RightButtonDown(e);
                    break;
                case GridInputMode.CreateLine:
                    CreateLine_RightButtonDown(e);
                    break;
                case GridInputMode.SetCenter:
                    SetCenter_RightButtonDown(e);
                    break;
                case GridInputMode.Dragging:
                    Dragging_RightButtonDown(e);
                    break;
            }
        }

        private void SphereGrid_OnPreviewMouseMove(object sender, MouseEventArgs e)
        {
            switch (InputMode)
            {
                case GridInputMode.Default:
                    Default_MouseMove(e);
                    break;
                case GridInputMode.CreateLine:
                    CreateLine_MouseMove(e);
                    break;
                case GridInputMode.SetCenter:
                    SetCenter_MouseMove(e);
                    break;
                case GridInputMode.Dragging:
                    Dragging_MouseMove(e);
                    break;
            }
        }

        #endregion

        #region Default input mode handlers

        private void Default_LeftButtonDown(MouseButtonEventArgs e)
        {
            if (Unlocked &&
                (e.OriginalSource is SphereGridNode ||
                 e.OriginalSource is SphereGridGlyph))
            {
                SetupForDrag(e);
            }

            if (_clickedTime > DateTime.Now - TimeSpan.FromMilliseconds(_doubleClickTime) &&
                Equals(e.OriginalSource, _clickedObject))
            {
                // Double click registered
                if (e.OriginalSource is SphereGridGlyph glyph)
                {
                    SelectedObject = glyph.Glyph;
                    ObjectFocused?.Invoke(glyph.Glyph);
                }
                else if (e.OriginalSource is SphereGridNode node)
                {
                    SelectedObject = node.Node;
                    ObjectFocused?.Invoke(node.Node);
                }
                else if (e.OriginalSource is Path ||
                         e.OriginalSource is Line)
                {
                    var line = _lines.First(l => Equals(l.Value.LineShape, e.OriginalSource));
                    SelectedObject = line.Key;
                    ObjectFocused?.Invoke(line.Key);
                }
                else
                {
                    SelectedObject = null;
                    ObjectFocused?.Invoke(null);
                }

                _clickedObject = null;
                return;
            }

            _clickedObject = e.OriginalSource;
            _clickedTime = DateTime.Now;
        }

        private void Default_LeftButtonUp(MouseButtonEventArgs e)
        {
            _dragStarting = false;
            IsDragging = false;
        }

        private void Default_MouseMove(MouseEventArgs e)
        {
            var mousePos = e.GetPosition(this);

            if (!_dragStarting)
                return;

            var moveDistance =
                Math.Sqrt(Math.Pow(mousePos.X - _dragStartPos.X, 2) + Math.Pow(mousePos.Y - _dragStartPos.Y, 2));
            if (moveDistance < 2)
                return;

            CaptureMouse();
            _dragStarting = false;

            SetInputMode(GridInputMode.Dragging);
        }

        private void SetupForDrag(MouseButtonEventArgs e)
        {
            _dragStarts.Clear();
            _dragObjects.Clear();
            _dragStartPos = e.GetPosition(this);
            _dragStarting = true;
            IsDragging = true;

            if (e.OriginalSource is SphereGridGlyph glyph)
            {
                _dragObjects.Add(glyph);
                _dragStarts[glyph] = new Point(glyph.Glyph.X, glyph.Glyph.Y);
                foreach (var node in glyph.Glyph.AttachedNodes)
                {
                    _dragStarts[_nodes[node]] = new Point(node.X, node.Y);
                    _dragObjects.Add(_nodes[node]);
                }
            }
            else if (e.OriginalSource is SphereGridNode node)
            {
                _dragStarts[node] = new Point(node.Node.X, node.Node.Y);
                _dragObjects.Add(node);
            }
            else
            {
                return;
            }
        }

        private void Default_RightButtonDown(MouseButtonEventArgs e)
        {
            // Show appropriate context menu on target
            ContextMenu contextMenu;
            if (e.OriginalSource is SphereGridNode node)
            {
                contextMenu = (ContextMenu) Resources["NodeContextMenu"];
                contextMenu.DataContext = node.Node;
            }
            else if (e.OriginalSource is SphereGridGlyph glyph)
            {
                contextMenu = (ContextMenu) Resources["GlyphContextMenu"];
                contextMenu.DataContext = glyph.Glyph;
            }
            else if (e.OriginalSource is Line ||
                     e.OriginalSource is Path)
            {
                contextMenu = (ContextMenu) Resources["LineContextMenu"];
                var line = _lines.First(l => Equals(l.Value.LineShape, e.OriginalSource));
                contextMenu.DataContext = line.Key;
            }
            else if (e.OriginalSource.GetType() == typeof(Border))
            {
                contextMenu = (ContextMenu) Resources["MainContextMenu"];
            }
            else
            {
                return;
            }

            contextMenu.PlacementTarget = (FrameworkElement) e.OriginalSource;
            contextMenu.IsOpen = true;
        }

        #endregion

        #region Create line input mode handlers

        private void CreateLine_LeftButtonDown(MouseButtonEventArgs e)
        {
            if (!(e.OriginalSource is SphereGridNode node))
                return;


            var line = _grid.AddLine(((GridNode) SelectedObject), node.Node);

            DrawLine(line);
            SetInputMode(GridInputMode.Default);

            SelectedObject = line;
        }

        private void CreateLine_RightButtonDown(MouseButtonEventArgs e)
        {
            // Cancel line creation
            SetInputMode(GridInputMode.Default);
        }

        private void CreateLine_MouseMove(MouseEventArgs mouseEventArgs)
        {
            var mousePos = mouseEventArgs.GetPosition(LineCanvas);
            ((Line) _newLine).X2 = mousePos.X;
            ((Line) _newLine).Y2 = mousePos.Y;
        }

        #endregion

        #region Set center input mode handlers

        private void SetCenter_LeftButtonDown(MouseEventArgs e)
        {
            if (!(e.OriginalSource is SphereGridNode gridNode))
                return;

            var line = (GridLine) SelectedObject;

            line.CenterNode = gridNode.Node;
            DrawLine(line);
            SetInputMode(GridInputMode.Default);
        }

        private void SetCenter_RightButtonDown(MouseEventArgs e)
        {
            SetInputMode(GridInputMode.Default);
        }

        private void SetCenter_MouseMove(MouseEventArgs e)
        {
            if (!(e.OriginalSource is SphereGridNode gridNode))
                return;

            var line = (GridLine) SelectedObject;

            var node1ToNode2 = Math.Atan2(line.ToNode.Y - line.FromNode.Y, line.ToNode.X - line.FromNode.X)
                               * 180.0 / Math.PI;

            // Find mid point between nodes
            var midPoint = new Point((double) (line.FromNode.X + line.ToNode.X) / 2,
                (double) (line.FromNode.Y + line.ToNode.Y) / 2);

            // Calculate angle from mid point to center node
            var midPointToCenter = Math.Atan2(gridNode.Node.Y - midPoint.Y, gridNode.Node.X - midPoint.X)
                                   * 180.0 / Math.PI;

            // Calculate size of curvature
            var size = Math.Sqrt(Math.Pow(gridNode.Node.X - line.FromNode.X, 2) +
                                 Math.Pow(gridNode.Node.Y - line.FromNode.Y, 2));

            var sweep = SweepDirection.Clockwise;

            Debug.WriteLine($"Node1->2: {node1ToNode2}, midToCenter: {midPointToCenter}");
            if (node1ToNode2 >= 0)
            {
                if (node1ToNode2 > 90)
                {
                    if (midPointToCenter > 0 ||
                        midPointToCenter < 0 && midPointToCenter > -90)
                        sweep = SweepDirection.Counterclockwise;
                }
                else if (node1ToNode2 < 90)
                {
                    if (midPointToCenter <= 0)
                        sweep = SweepDirection.Counterclockwise;
                }
                else
                {
                    if (Math.Abs(midPointToCenter) < 90)
                        sweep = SweepDirection.Counterclockwise;
                }
            }
            else
            {
                if (node1ToNode2 < -90)
                {
                    if (midPointToCenter > 0)
                        sweep = SweepDirection.Counterclockwise;
                }
                else if (node1ToNode2 > -90)
                {
                    if (midPointToCenter <= 0)
                        sweep = SweepDirection.Counterclockwise;
                }
                else
                {
                    if (Math.Abs(midPointToCenter) > 90)
                        sweep = SweepDirection.Counterclockwise;
                }
            }

            _pathSegment.Size = new Size(size, size);
            _pathSegment.SweepDirection = sweep;
        }

        #endregion

        #region Dragging input mode handlers

        private void Dragging_LeftButtonUp(MouseEventArgs e)
        {
            // Drag completed
            IsDragging = false;
            SetInputMode(GridInputMode.Default);

            // Ensure elements are in final position
            foreach (var element in _dragObjects)
            {
                if (element is SphereGridNode node)
                {
                    Canvas.SetLeft(node, node.Node.X);
                    Canvas.SetTop(node, node.Node.Y);
                }
                else if (element is SphereGridGlyph glyph)
                {
                    Canvas.SetLeft(glyph, glyph.Glyph.X);
                    Canvas.SetTop(glyph, glyph.Glyph.Y);
                }
            }
        }

        private void Dragging_RightButtonDown(MouseEventArgs e)
        {
            // Drag canceled
            IsDragging = false;
            SetInputMode(GridInputMode.Default);

            // Set elements back to default position
            foreach (var element in _dragObjects)
            {
                if (element is SphereGridNode node)
                {
                    node.Node.X = (short) _dragStarts[node].X;
                    node.Node.Y = (short) _dragStarts[node].Y;
                    Canvas.SetLeft(node, node.Node.X);
                    Canvas.SetTop(node, node.Node.Y);
                }
                else if (element is SphereGridGlyph glyph)
                {
                    glyph.Glyph.X = (short) _dragStarts[glyph].X;
                    glyph.Glyph.Y = (short) _dragStarts[glyph].Y;
                    Canvas.SetLeft(glyph, glyph.Glyph.X);
                    Canvas.SetTop(glyph, glyph.Glyph.Y);
                }
            }
        }

        private void Dragging_MouseMove(MouseEventArgs e)
        {
            // Move elements according to mouse position
            var mousePos = e.GetPosition(this);
            var drawnLines = new List<GridLine>();
            foreach (var element in _dragObjects)
            {
                if (element is SphereGridNode node)
                {
                    node.Node.X = (short) (_dragStarts[node].X + mousePos.X - _dragStartPos.X);
                    node.Node.Y = (short) (_dragStarts[node].Y + mousePos.Y - _dragStartPos.Y);
                    Canvas.SetLeft(node, node.Node.X);
                    Canvas.SetTop(node, node.Node.Y);
                    foreach (var line in node.Node.LinkedNodes)
                    {
                        if (drawnLines.Contains(line.Key))
                            continue;

                        DrawLine(line.Key);
                        drawnLines.Add(line.Key);
                    }
                }
                else if (element is SphereGridGlyph glyph)
                {
                    glyph.Glyph.X = (short) (_dragStarts[glyph].X + mousePos.X - _dragStartPos.X);
                    glyph.Glyph.Y = (short) (_dragStarts[glyph].Y + mousePos.Y - _dragStartPos.Y);
                    Canvas.SetLeft(glyph, glyph.Glyph.X);
                    Canvas.SetTop(glyph, glyph.Glyph.Y);
                }
            }
        }

        #endregion

        #region Node context menu

        private void MenuNode_CreateLink_OnClick(object sender, RoutedEventArgs e)
        {
            var node = GetContextNode(e);
            if (node == null) return;

            SelectedObject = node;
            _newLine = new Line
            {
                Stroke = Brushes.Aqua,
                StrokeThickness = 4
            };

            ((Line) _newLine).X2 = ((Line) _newLine).X1 = node.X;
            ((Line) _newLine).Y2 = ((Line) _newLine).Y1 = node.Y;

            LineCanvas.Children.Add(_newLine);
            SetInputMode(GridInputMode.CreateLine);
        }

        private void MenuNode_SetPathStart_OnClick(object sender, RoutedEventArgs e)
        {
            var node = GetContextNode(e);
            if (node == null) return;

            _startNode = node;
            DrawShortestPath();
        }

        private void MenuNode_SetPathEnd_OnClick(object sender, RoutedEventArgs e)
        {
            var node = GetContextNode(e);
            if (node == null) return;

            _endNode = node;
            DrawShortestPath();
        }

        private void MenuNode_ShowBranches_OnClick(object sender, RoutedEventArgs e)
        {
            var node = GetContextNode(e);
            if (node == null) return;

            _startNode = node;
            _endNode = null;
            ClearPaths();
            DrawBranches();
        }

        private void MenuNode_ClearPaths_OnClick(object sender, RoutedEventArgs e)
        {
            _startNode = null;
            _endNode = null;
            ClearPaths();
        }

        private GridNode GetContextNode(RoutedEventArgs e)
        {
            var contextMenu = ((FrameworkElement) e.OriginalSource).TryFindParent<ContextMenu>();
            return contextMenu.DataContext as GridNode;
        }

        #endregion

        #region Line context menu

        private PathFigure _pathFigure;
        private ArcSegment _pathSegment;
        private List<FrameworkElement> _dragObjects = new List<FrameworkElement>();
        private Point _dragStartPos;
        private Dictionary<object, Point> _dragStarts = new Dictionary<object, Point>();
        private bool _dragStarting;

        private void MenuLine_SetCenter_OnClick(object sender, RoutedEventArgs e)
        {
            var line = GetContextLine(e);
            if (line == null)
                return;

            _pathSegment = new ArcSegment
            {
                Point = new Point(line.ToNode.X, line.ToNode.Y),
                Size = new Size(0, 0),
                SweepDirection = SweepDirection.Clockwise
            };

            _pathFigure = new PathFigure
            {
                StartPoint = new Point(line.FromNode.X, line.FromNode.Y),
                Segments =
                {
                    _pathSegment
                }
            };

            _newLine = new Path
            {
                Data = new PathGeometry
                {
                    Figures =
                    {
                        _pathFigure
                    }
                },
                Stroke = Brushes.Violet,
                StrokeThickness = 4
            };

            LineCanvas.Children.Add(_newLine);
            SelectedObject = line;
            SetInputMode(GridInputMode.SetCenter);
        }

        private void MenuLine_DeleteLink_OnClick(object sender, RoutedEventArgs e)
        {
            var line = GetContextLine(e);
            if (line == null)
                return;

            _grid.DeleteLine(line);
            DrawLine(line);
        }

        private GridLine GetContextLine(RoutedEventArgs e)
        {
            var contextMenu = ((FrameworkElement) e.OriginalSource).TryFindParent<ContextMenu>();
            return contextMenu.DataContext as GridLine;
        }

        #endregion

        #region Path drawing

        private void ClearPaths()
        {
            _pathIndex = 0;
            _pathHistory.Clear();
            _pathLines.Clear();
            _pathNodes?.Clear();
            foreach (var line in _lines)
            {
                line.Value.ClearValue(SphereGridLine.IsHighlightedProperty);
                line.Value.ClearValue(SphereGridLine.HighlightBrushProperty);
            }

            foreach (var node in _nodes)
                node.Value.ClearValue(SphereGridNode.IsActiveProperty);
        }

        private void DrawShortestPath()
        {
            if (_startNode == null || _endNode == null)
                return;

            ClearPaths();
            var path = _grid.ShortestPath(_startNode, _endNode, _pathLockLevel);
            if (path == null)
            {
                // TODO: Show error message
            }
            else
            {
                path.Reverse();
                ClearPaths();
                _pathNodes = path;
                AnimateNextNode();
            }
        }

        private void DrawBranches()
        {
            var branches = _grid.CalculateBranches(_startNode, _pathLockLevel);
            if (branches == null || branches.Count == 0)
                return;

            _pathNodes = branches;
            AnimateNextNode();
        }

        private void AnimateNextNode()
        {
            if (_pathIndex == _pathNodes.Count)
                return;

            var node = _pathNodes[_pathIndex++];
            var nodeControl = _nodes[node];
            nodeControl.IsActive = true;

            _pathHistory.Add(node);

            var line = node.LinkedNodes.FirstOrDefault(l =>
                _pathHistory.Contains(l.Value) && !_pathLines.Contains(l.Key));

            if (line.Value != null)
            {
                _pathLines.Add(line.Key);
                // Set color to black and highlight to active
                _lines[line.Key].HighlightColor = Colors.Black;
                _lines[line.Key].IsHighlighted = true;

                // Create animation to fade in line
                var da = new ColorAnimation(Colors.Black, Colors.LawnGreen, TimeSpan.FromMilliseconds(1));
                da.Completed += (sender, args) =>
                {
                    // Fade in next line on completion
                    AnimateNextNode();
                };

                _lines[line.Key].BeginAnimation(SphereGridLine.HighlightColorProperty, da);
            }
            else
                AnimateNextNode();
        }

        #endregion

        private void SetInputMode(GridInputMode mode)
        {
            switch (mode)
            {
                case GridInputMode.Default:
                    ReleaseMouseCapture();
                    LineCanvas.Children.Remove(_newLine);
                    break;
                case GridInputMode.CreateLine:
                    break;
                case GridInputMode.SetCenter:
                    break;
            }

            InputMode = mode;
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public enum GridInputMode
    {
        Default,
        CreateLine,
        SetCenter,
        Dragging
    }
}