﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Shapes;
using FFXLib;
using Omega.Classes.SphereGrid;

namespace Omega.FFX.SphereGrid.Controls
{
    public class SphereGridLine : FrameworkElement, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public SphereGrid Grid { get; }
        public GridLine Line { get; private set; }

        public Shape LineShape { get; private set; }
        public Brush StrokeBrush { get; private set; }

        #region Dependency Properties

        public bool IsHighlighted
        {
            get => (bool) GetValue(IsHighlightedProperty);
            set => SetValue(IsHighlightedProperty, value);
        }

        public bool IsEnlarged
        {
            get => (bool) GetValue(IsEnlargedProperty);
            set => SetValue(IsEnlargedProperty, true);
        }

        public Color HighlightColor
        {
            get
            {
                if (HighlightBrush is SolidColorBrush solidColorBrush)
                {
                    return solidColorBrush.Color;
                }

                return Colors.Black;
            }
            set => SetValue(HighlightBrushProperty, new SolidColorBrush(value));
        }

        public Brush HighlightBrush
        {
            get => (Brush) GetValue(HighlightBrushProperty);
            set => SetValue(HighlightBrushProperty, value);
        }

        public static DependencyProperty IsHighlightedProperty = DependencyProperty.Register("IsHighlighted",
            typeof(bool), typeof(SphereGridLine), new FrameworkPropertyMetadata(false, OnIsHighlightedChanged));

        public static DependencyProperty HighlightBrushProperty = DependencyProperty.Register("HighlightBrush",
            typeof(Brush), typeof(SphereGridLine),
            new FrameworkPropertyMetadata(Brushes.White, OnHighlightBrushChanged));

        public static DependencyProperty HighlightColorProperty = DependencyProperty.Register("HighlightColor",
            typeof(Color), typeof(SphereGridLine),
            new FrameworkPropertyMetadata(Colors.White, OnHighlightColorChanged));

        public static DependencyProperty IsEnlargedProperty = DependencyProperty.Register("IsEnlarged",
            typeof(bool), typeof(SphereGridLine), new FrameworkPropertyMetadata(false, OnIsEnlargedChanged));

        private static void OnHighlightBrushChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) =>
            ((SphereGridLine) d).HighlightBrush = (Brush) e.NewValue;

        private static void OnHighlightColorChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) =>
            ((SphereGridLine) d).HighlightColor = (Color) e.NewValue;

        private static void OnIsHighlightedChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) =>
            ((SphereGridLine) d).IsHighlighted = (bool) e.NewValue;

        private static void OnIsEnlargedChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) =>
            ((SphereGridLine) d).IsEnlarged = (bool) e.NewValue;

        #endregion

        public SphereGridLine(GridLine line, SphereGrid grid)
        {
            Grid = grid;
            Line = line;
            if (Line.CenterNode != null)
            {
                // Draw curved line

                // Get angle between nodes, 0 is east, negative values north, positive values south
                var node1ToNode2 = Math.Atan2(Line.ToNode.Y - Line.FromNode.Y, Line.ToNode.X - Line.FromNode.X)
                                   * 180.0 / Math.PI;

                // Find mid point between nodes
                var midPoint = new Point((double) (Line.FromNode.X + Line.ToNode.X) / 2,
                    (double) (Line.FromNode.Y + Line.ToNode.Y) / 2);

                // Calculate angle from mid point to center node
                var midPointToCenter = Math.Atan2(Line.CenterNode.Y - midPoint.Y, Line.CenterNode.X - midPoint.X)
                                       * 180.0 / Math.PI;

                // Calculate size of curvature
                var size = Math.Sqrt(Math.Pow(Line.CenterNode.X - Line.FromNode.X, 2) +
                                     Math.Pow(Line.CenterNode.Y - Line.FromNode.Y, 2));

                var sweep = SweepDirection.Clockwise;

                Point startPoint, endPoint; // Start and end points for the gradient

                if (node1ToNode2 >= 0)
                {
                    if (node1ToNode2 > 90)
                    {
                        startPoint = new Point(1, 0);
                        endPoint = new Point(0, 1);
                        if (midPointToCenter > 0)
                            sweep = SweepDirection.Counterclockwise;
                    }
                    else if (node1ToNode2 < 90)
                    {
                        startPoint = new Point(0, 0);
                        endPoint = new Point(1, 1);
                        if (midPointToCenter <= 0)
                            sweep = SweepDirection.Counterclockwise;
                    }
                    else
                    {
                        startPoint = new Point(0.5, 0);
                        endPoint = new Point(0.5, 1);
                        if (Math.Abs(midPointToCenter) < 90)
                            sweep = SweepDirection.Counterclockwise;
                    }
                }
                else
                {
                    if (node1ToNode2 < -90)
                    {
                        startPoint = new Point(1, 1);
                        endPoint = new Point(0, 0);
                        if (midPointToCenter > 0)
                            sweep = SweepDirection.Counterclockwise;
                    }
                    else if (node1ToNode2 > -90)
                    {
                        startPoint = new Point(0, 1);
                        endPoint = new Point(1, 0);
                        if (midPointToCenter <= 0)
                            sweep = SweepDirection.Counterclockwise;
                    }
                    else
                    {
                        startPoint = new Point(0.5, 0);
                        endPoint = new Point(0.5, 1);
                        if (Math.Abs(midPointToCenter) > 90)
                            sweep = SweepDirection.Counterclockwise;
                    }
                }

                // Create gradient stroke brush
                StrokeBrush = new LinearGradientBrush(Colors.Blue, Colors.Red, startPoint, endPoint);
                
                // Create the actual shape object
                LineShape = new Path
                {
                    Data = new PathGeometry
                    {
                        Figures =
                        {
                            new PathFigure
                            {
                                StartPoint = new Point(Line.FromNode.X, Line.FromNode.Y),
                                Segments =
                                {
                                    new ArcSegment
                                    {
                                        Point = new Point(Line.ToNode.X, Line.ToNode.Y),
                                        Size = new Size(size, size),
                                        SweepDirection = sweep
                                    }
                                }
                            }
                        }
                    }
                };
            }
            else
            {
                var node1ToNode2 = Math.Atan2(Line.ToNode.Y - Line.FromNode.Y, Line.ToNode.X - Line.FromNode.X)
                                   * 180.0 / Math.PI;
                Point startPoint, endPoint;

                if (node1ToNode2 >= 0)
                {
                    if (node1ToNode2 > 90)
                    {
                        startPoint = new Point(1, 0);
                        endPoint = new Point(0, 1);
                    }
                    else if (node1ToNode2 < 90)
                    {
                        startPoint = new Point(0, 0);
                        endPoint = new Point(1, 1);
                    }
                    else
                    {
                        startPoint = new Point(0.5, 0);
                        endPoint = new Point(0.5, 1);
                    }
                }
                else
                {
                    if (node1ToNode2 < -90)
                    {
                        startPoint = new Point(1, 1);
                        endPoint = new Point(0, 0);
                    }
                    else if (node1ToNode2 > -90)
                    {
                        startPoint = new Point(0, 1);
                        endPoint = new Point(1, 0);
                    }
                    else
                    {
                        startPoint = new Point(0.5, 0);
                        endPoint = new Point(0.5, 1);
                    }
                }

                StrokeBrush = new LinearGradientBrush(Colors.Blue, Colors.Red,
                    startPoint, endPoint);
                LineShape = new Line
                {
                    X1 = Line.FromNode.X,
                    Y1 = Line.FromNode.Y,
                    X2 = Line.ToNode.X,
                    Y2 = Line.ToNode.Y
                };
            }

            LineShape.Style = new Style
            {
                Setters =
                {
                    new Setter
                    {
                        Property = Shape.StrokeProperty,
                        Value = Brushes.Black
                    },
                    new Setter
                    {
                        Property = Shape.StrokeThicknessProperty,
                        Value = 6.0
                    }
                },
                Triggers =
                {
                    new DataTrigger
                    {
                        Binding = new Binding
                        {
                            Source = this,
                            Path = new PropertyPath(IsEnlargedProperty)
                        },
                        Value = true,
                        Setters =
                        {
                            new Setter(Shape.StrokeThicknessProperty, 8.0)
                        }
                    },
                    new DataTrigger
                    {
                        Binding = new Binding
                        {
                            Source = this,
                            Path = new PropertyPath(IsHighlightedProperty)
                        },
                        Value = true,
                        Setters =
                        {
                            new Setter(Shape.StrokeProperty, new Binding
                            {
                                Source = this,
                                Path = new PropertyPath(HighlightBrushProperty)
                            })
                        }
                    },
                    new DataTrigger
                    {
                        Binding = new Binding
                        {
                            Source = Grid,
                            Path = new PropertyPath("SelectedObject")
                        },
                        Value = Line,
                        Setters =
                        {
                            new Setter(EffectProperty, new DropShadowEffect
                            {
                                Color = Colors.Gold,
                                ShadowDepth = 0,
                                BlurRadius = 20
                            })
                        }
                    }
                }
            };

            Style = new Style
            {
                Setters =
                {
                    new Setter(HighlightBrushProperty, StrokeBrush)
                },
                Triggers =
                {
                    new DataTrigger
                    {
                        Binding = new Binding
                        {
                            Source = LineShape,
                            Path = new PropertyPath(IsMouseOverProperty)
                        },
                        Value = true,
                        Setters =
                        {
                            new Setter(IsHighlightedProperty, true),
                            new Setter(IsEnlargedProperty, true),
                            new Setter(HighlightBrushProperty, StrokeBrush)
                        }
                    },
                    new DataTrigger
                    {
                        Binding = new Binding
                        {
                            Source = Grid,
                            Path = new PropertyPath("SelectedObject")
                        },
                        Value = Line,
                        Setters =
                        {
                            new Setter(IsHighlightedProperty, true),
                            new Setter(HighlightBrushProperty, Brushes.Gold)
                        }
                    }
                }
            };
        }


        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}