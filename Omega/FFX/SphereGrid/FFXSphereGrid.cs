﻿using FFXLib.FileSystem.Data;
using Omega.Classes.SphereGrid;

namespace Omega.FFX.SphereGrid
{
    public class FFXSphereGrid
    {

        public static Grid Standard { get; private set; }
        public static Grid Expert { get; private set; }

        public FFXSphereGrid()
        {

        }

        public static void Load()
        {
            Standard = new Grid(GridType.Standard, new SphereGridFile(FFXData.Files.GetStream("ffx_ps2/ffx/master/jppc/menu/abmap/dat02.dat")));
            Expert = new Grid(GridType.Expert, new SphereGridFile(FFXData.Files.GetStream("ffx_ps2/ffx/master/jppc/menu/abmap/dat03.dat")));
        }
    }
}