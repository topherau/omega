﻿using System.IO;
using FFXLib.Enums;
using FFXLib.FileSystem;
using FFXLib.FileSystem.Data;
using FFXLib.FileSystem.Data.Records;
using Omega.FFX.SphereGrid;

namespace Omega.FFX
{
    public static class FFXData
    {
        public static bool IsLoaded { get; private set; }
        public static FileManager Files { get; private set; }
        public static StringConverter StringConverter { get; private set; }
        public static bool Load()
        {
            var gameFile = Path.Combine(App.Config.GameDirectory, "data\\FFX_Data.vbf");
            if (App.Config.GameDirectory == null || !File.Exists(gameFile))
                throw new FileNotFoundException();

            Files = new FileManager(Localization.English, gameFile, App.Config.WorkDirectory);
            StringConverter = Files.StringConverter;

            Record.StringConverter = StringConverter;

            FFXStrings.Load();
            StringConverter.MacroDictionary = FFXStrings.MacroDictionary;
            FFXKernel.Load();
            FFXBattle.Load();
            FFXEvent.Load();
            FFXSphereGrid.Load();

            IsLoaded = true;
            return true;
        }

        public static bool Unload()
        {
            Files = null;
            StringConverter = null;
            Record.StringConverter = null;

            FFXKernel.Unload();
            IsLoaded = false;
            return true;
        }

        public static bool IsDirty()
        {
            if (!IsLoaded)
                return false;

            return true;
        }
    }
}