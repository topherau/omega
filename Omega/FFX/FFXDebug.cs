﻿using System;
using System.Diagnostics;
using System.Runtime.ConstrainedExecution;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Cryptography;
using System.Text;

namespace Omega.FFX
{
    public static class FFXDebug
    {
        #region Static imports

        private const int ProcessWmRead = 0x0010;
        private const int ProcessVmWrite = 0x0020;
        private const int ProcessVmOperation = 0x0008;

        [DllImport("kernel32.dll")]
        private static extern IntPtr OpenProcess(int dwDesiredAccess, bool bInheritHandle, int dwProcessId);

        [DllImport("kernel32.dll")]
        private static extern bool ReadProcessMemory(IntPtr hProcess, IntPtr lpBaseAddress, byte[] lpBuffer, int dwSize,
            ref int lpNumberOfBytesRead);

        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern bool WriteProcessMemory(IntPtr hProcess, IntPtr lpBaseAddress, byte[] lpBuffer,
            int dwSize,
            ref int lpNumberOfBytesWritten);

        [DllImport("Kernel32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        private static extern uint GetFinalPathNameByHandle(IntPtr hFile,
            [MarshalAs(UnmanagedType.LPTStr)] StringBuilder lpszFilePath, uint cchFilePath, uint dwFlags);

        [DllImport("kernel32.dll", CharSet = CharSet.Unicode, SetLastError = true,
            CallingConvention = CallingConvention.StdCall)]
        private static extern IntPtr CreateFileW(string filename, uint desiredAccess, uint shareMode,
            IntPtr securityAttributes,
            uint creationDisposition, uint flagsAndAttributes, IntPtr templateFile);

        [DllImport("Kernel32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        static extern uint SetFilePointer(
            IntPtr hFile,
            int lDistanceToMove,
            ref IntPtr lpDistanceToMoveHigh,
            uint dwMoveMethod);

        [DllImport("kernel32.dll", SetLastError = true, CallingConvention = CallingConvention.StdCall)]
        private static extern bool SetFilePointerEx(IntPtr hFile, long liDistanceToMove, IntPtr lpNewFilePointer,
            uint dwMoveMethod);

        [DllImport("kernel32.dll", SetLastError = true, CallingConvention = CallingConvention.StdCall)]
        private static extern bool ReadFile(IntPtr hFile, IntPtr lpBuffer, uint nNumberOfBytesToRead,
            out uint lpNumberOfBytesRead,
            IntPtr lpOverlapped);

        [DllImport("kernel32.dll", SetLastError = true)]
        [ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
        [SuppressUnmanagedCodeSecurity]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool CloseHandle(IntPtr hObject);

        #endregion

        private const string _checkHash = "AD6F7A591656093426F7B5CC1E644CAB";

        private static readonly MD5 _md5 = MD5.Create();

        private static IntPtr _handle;
        private static Process _process;

        public static bool TriggerBattle(ushort mapId, byte battleId)
        {
            WriteBytes(0xD2CA38, BitConverter.GetBytes(1)); //           Enable random battles
            WriteBytes(0xD2C256, BitConverter.GetBytes(mapId)); //       Set map ID
            WriteBytes(0xD2C258, BitConverter.GetBytes((byte) 4)); //    Unknown value passed to initiateBattle
            WriteBytes(0xD2C259, BitConverter.GetBytes(battleId)); //    Set encounter ID
            WriteBytes(0xD2A8E2, BitConverter.GetBytes((byte) 1)); //    Set to 1 to trigger random battle
            return true;
        }

        public static bool WriteItems() => WriteBytesToPointer(0xD2A940, FFXKernel.GetItemBytes());
        public static bool WriteCommands() => WriteBytesToPointer(0xD2A92C, FFXKernel.GetCommandBytes());
        public static bool WriteAutoAbilities() => WriteBytesToPointer(0xD2A92C, FFXKernel.GetAutoAbilityBytes());
        public static bool WriteCustomizations() => WriteBytesToPointer(0, FFXKernel.GetCustomizationBytes());

        public static bool ValidateProcess(Process process)
        {
            var handle = OpenProcess(ProcessWmRead, false, process.Id);
            if (handle == IntPtr.Zero)
                throw new Exception("Unable to open process for reading.");

            try
            {
                var buffer = new byte[0x1000];
                var bytesRead = 0;
                var readMemory = ReadProcessMemory(handle, process.MainModule.BaseAddress, buffer, buffer.Length,
                    ref bytesRead);
                if (!readMemory)
                    throw new Exception("Unable to read process memory.");

                var hash = _md5.ComputeHash(buffer);

                var hashString = BitConverter.ToString(hash).Replace("-", "");
                if (hashString != _checkHash)
                    return false;

                return true;
            }
            finally
            {
                CloseHandle(handle);
            }
        }

        public static bool Attach(Process process)
        {
            if (_handle != IntPtr.Zero)
            {
                CloseHandle(_handle);
                _handle = IntPtr.Zero;
            }

            var handle = OpenProcess(ProcessWmRead | ProcessVmWrite | ProcessVmOperation, false, process.Id);

            if (handle == IntPtr.Zero)
                return false;

            _handle = handle;
            _process = process;

            return true;
        }

        private static bool CheckProcess()
        {
            return _process != null && !_process.HasExited;
        }

        private static bool WriteBytes(int offset, byte[] bytes)
        {
            if (!CheckProcess())
                return false;

            var bytesWritten = 0;
            return WriteProcessMemory(_handle, _process.MainModule.BaseAddress + offset, bytes, bytes.Length,
                ref bytesWritten);
        }

        private static bool WriteBytesToPointer(int offset, byte[] bytes)
        {
            if (!CheckProcess())
                return false;

            var pBuffer = new byte[4];
            var bytesRead = 0;
            var readMemory = ReadProcessMemory(_handle, _process.MainModule.BaseAddress + offset, pBuffer, 4,
                ref bytesRead);
            if (!readMemory)
                return false;

            var pointer = (IntPtr)BitConverter.ToUInt32(pBuffer, 0);
            var bytesWritten = 0;
            var wroteMemory = WriteProcessMemory(_handle, pointer, bytes, bytes.Length, ref bytesWritten);
            return wroteMemory;
        }
    }
}