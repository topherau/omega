﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using FFXLib.Classes;
using FFXLib.Extension;
using FFXLib.FileSystem.Data.Files;

namespace Omega.FFX
{
    public static class FFXEvent
    {
        private const string EventIdFile = "ffx_ps2/ffx/proj/event/header/eventid.bin";
        private const string EventStringFile = "ffx_ps2/ffx/master/$LANG$/event/obj_ps3/$EVENT$.bin";
        
        public static ObservableCollection<string> Events { get; set; }
        public static ObservableDictionary<string, StringTableFile> EventStrings { get; set; }

        public static void Load()
        {
            LoadEventNames(FFXData.Files.GetStream(EventIdFile));
            LoadEvents();
        }

        private static void LoadEventNames(MemoryStream stream)
        {
            var offsetList = new List<uint>();
            var lengthList = new List<uint>();

            offsetList.Add(stream.ReadUInt32());
            lengthList.Add(stream.ReadUInt32());

            while (stream.Position < offsetList[0])
            {
                offsetList.Add(stream.ReadUInt32());
                lengthList.Add(stream.ReadUInt32());
            }

            var fieldList = new List<string>();
            for (var i = 0; i < offsetList.Count; i++)
            {
                stream.Seek(offsetList[i], SeekOrigin.Begin);
                var str = stream.ReadBytes((int) lengthList[i]);

                fieldList.Add(Encoding.ASCII.GetString(str).TrimEnd('\0'));
            }

            Events = new ObservableCollection<string>(fieldList);
        }

        private static void LoadEvents()
        {
            var evDic = new Dictionary<string, StringTableFile>();
            foreach (var ev in Events)
            {
                if (string.IsNullOrEmpty(ev))
                    continue;
                var evFolder = ev.Substring(0, 2);
                var folderPath = EventStringFile.Replace("$EVENT$", $"{evFolder}/{ev}/{ev}");
                var evFile = FFXData.Files.GetStream(FFXData.Files.GetLangPath(folderPath));
                if (evFile == null)
                    continue;

                evDic[ev] = new StringTableFile(evFile);
            }

            EventStrings = new ObservableDictionary<string, StringTableFile>(evDic);
        }
    }
}