﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using FFXLib.Classes;
using FFXLib.Enums.Flags;
using FFXLib.FileSystem.Data;
using FFXLib.FileSystem.Data.Records;
using FFXLib.FileSystem.Data.Records.FFX.Kernel;

namespace Omega.FFX
{
    public static class FFXKernel
    {
        public static ObservableCollection<Item> Items { get; private set; }
        public static ObservableCollection<Command> Commands { get; private set; }
        public static ObservableCollection<MonMagic> MonMagic { get; private set; }
        public static ObservableCollection<AutoAbility> AutoAbility { get; private set; }
        public static ObservableCollection<Kaizou> Customization { get; private set; }
        public static ObservableCollection<Monster> Monster { get; private set; }
        public static ObservableCollection<Sphere> Spheres { get; private set; }
        public static ObservableCollection<Panel> Nodes { get; private set; }

        public static ObservableDictionary<ushort, string> ItemDictionary { get; private set; }
        public static ObservableDictionary<ushort, string> CommandDictionary { get; private set; }
        public static ObservableDictionary<ushort, string> MonMagicDictionary { get; private set; }
        public static ObservableDictionary<ushort, string> AutoAbilityDictionary { get; private set; }
        public static ObservableDictionary<ushort, string> CustomizationDictionary { get; private set; }
        public static ObservableDictionary<ushort, string> MonsterDictionary { get; private set; }
        public static ObservableDictionary<ushort, string> SphereDictionary { get; private set; }
        public static ObservableDictionary<ushort, string> NodeDictionary { get; private set; }

        private const string ItemFile = "ffx_ps2/ffx/master/$LANG$/battle/kernel/item.bin";
        private const string CommandFile = "ffx_ps2/ffx/master/$LANG$/battle/kernel/command.bin";
        private const string MonMagic1File = "ffx_ps2/ffx/master/$LANG$/battle/kernel/monmagic1.bin";
        private const string MonMagic2File = "ffx_ps2/ffx/master/$LANG$/battle/kernel/monmagic2.bin";
        private const string AutoAbilityFile = "ffx_ps2/ffx/master/$LANG$/battle/kernel/a_ability.bin";
        private const string CustomizationFile = "ffx_ps2/ffx/master/jppc/battle/kernel/kaizou.bin";

        private const string Monster1File = "ffx_ps2/ffx/master/$LANG$/battle/kernel/monster1.bin";
        private const string Monster2File = "ffx_ps2/ffx/master/$LANG$/battle/kernel/monster2.bin";
        private const string Monster3File = "ffx_ps2/ffx/master/$LANG$/battle/kernel/monster3.bin";
        
        private const string SphereFile = "ffx_ps2/ffx/master/$LANG$/battle/kernel/sphere.bin";
        private const string PanelFile = "ffx_ps2/ffx/master/$LANG$/battle/kernel/panel.bin";

        private const string PlySaveFile = "ffx_ps2/ffx/master/$LANG$/battle/kernel/ply_save.bin";
        private const string PlyRomFile = "ffx_ps2/ffx/master/$LANG$/battle/kernel/ply_rom.bin";
        private const string SaveTxtFile = "ffx_ps2/ffx/master/$LANG$/battle/kernel/save_txt.bin";

        private static KernelFile<Item> _items;
        private static KernelFile<Command> _commands;
        private static KernelFile<MonMagic> _monMagic1;
        private static KernelFile<MonMagic> _monMagic2;
        private static KernelFile<AutoAbility> _autoAbility;
        private static KernelFile<Kaizou> _customizations;

        private static KernelFile<Monster> _monster1;
        private static KernelFile<Monster> _monster2;
        private static KernelFile<Monster> _monster3;

        private static KernelFile<Sphere> _spheres;
        private static KernelFile<Panel> _nodes;

        private static KernelFile<PlySave> _plySave;
        private static KernelFile<PlyRom> _plyRom;
        private static KernelFile<SaveTxt> _saveTxt;


        public static void Load()
        {
            LoadItems();
            LoadCommands();
            LoadMonMagic();
            LoadAutoAbilities();
            LoadCustomizations();
            LoadMonster();
            LoadSpheres();
            LoadNodes();
            LoadPlySave();
            LoadPlyRom();
            LoadSaveTxt();
        }

        public static void Unload()
        {
            UnloadItems();
            UnloadCommands();
            UnloadSpheres();
            UnloadNodes();
        }

        public static void Save()
        {
            SaveItems();
        }

        #region Loaders

        private static void LoadItems()
        {
            _items = new KernelFile<Item>(FFXData.Files.GetStream(ItemFile));
            Items = new ObservableCollection<Item>(_items.Records);
            ItemDictionary = new ObservableDictionary<ushort, string>(
                new SortedDictionary<ushort, string>(
                        _items.Records.ToDictionary(r => (ushort) (0x2000 | _items.IndexOf(r)),
                            r => r.Name))
                    {[0] = "(None)"});

            _items.RecordPropertyChanged += (index, name) =>
            {
                var property = _items.RecordType.GetProperty(name);
                var propertyValue = property.GetValue(_items[index]);

                // Update item and sphere name when changed
                if (name == "Name")
                {
                    var sphereTypeProperty = _items.RecordType.GetProperty("SphereType");
                    var sphereTypeValue = sphereTypeProperty.GetValue(_items[index]);

                    ItemDictionary[(ushort) (0x2000 | index)] = (string) propertyValue;
                    if ((byte) sphereTypeValue < _spheres.Records.Count)
                    {
                        SphereDictionary[(byte) sphereTypeValue] = (string) propertyValue;
                    }
                }

                // Rebuild sphere dictionary when sphere type is changed
                if (name == "SphereType")
                {
                    RebuildSphereDictionary();
                }
            };
        }

        private static void LoadCommands()
        {
            _commands = new KernelFile<Command>(FFXData.Files.GetStream(CommandFile));
            Commands = new ObservableCollection<Command>(_commands.Records);
            CommandDictionary = new ObservableDictionary<ushort, string>(
                new SortedDictionary<ushort, string>(
                        _commands.Records.ToDictionary(r => (ushort) (0x3000 | _commands.IndexOf(r)),
                            r => r.Name))
                    {[0] = "(None)"});

            _commands.RecordPropertyChanged += (index, name) =>
            {
                if (name != "Name")
                    return;

                var property = _commands.RecordType.GetProperty(name);
                var newValue = property?.GetValue(_commands[index]) ?? string.Empty;
                CommandDictionary[(ushort) (0x3000 | index)] = (string) newValue;
            };
        }

        private static void LoadMonMagic()
        {
            _monMagic1 = new KernelFile<MonMagic>(FFXData.Files.GetStream(MonMagic1File));
            _monMagic2 = new KernelFile<MonMagic>(FFXData.Files.GetStream(MonMagic2File));

            MonMagic = new ObservableCollection<MonMagic>(_monMagic1.Records.Concat(_monMagic2.Records));

            var mm1Dic = _monMagic1.Records.ToDictionary(r => 0x4000 | _monMagic1.Records.IndexOf(r), r => r.Name);
            var mm2Dic = _monMagic2.Records.ToDictionary(r => 0x6000 | _monMagic2.Records.IndexOf(r), r => r.Name);
            MonMagicDictionary = new ObservableDictionary<ushort, string>(
                new SortedDictionary<ushort, string>(
                    mm1Dic.Concat(mm2Dic).ToDictionary(r => (ushort) r.Key, r => r.Value))
                {
                    [0] = "(None)"
                });

            _monMagic1.RecordPropertyChanged += (index, name) =>
            {
                if (name != "Name")
                    return;

                var property = _monMagic1.RecordType.GetProperty(name);
                var newValue = property?.GetValue(_monMagic1[index]) ?? string.Empty;
                MonMagicDictionary[(ushort) (0x4000 | index)] = (string) newValue;
            };

            _monMagic2.RecordPropertyChanged += (index, name) =>
            {
                if (name != "Name")
                    return;

                var property = _monMagic2.RecordType.GetProperty(name);
                var newValue = property?.GetValue(_monMagic2[index]) ?? string.Empty;
                MonMagicDictionary[(ushort) (0x6000 | index)] = (string) newValue;
            };
        }
        
        private static void LoadAutoAbilities()
        {
            _autoAbility = new KernelFile<AutoAbility>(FFXData.Files.GetStream(AutoAbilityFile));

            AutoAbility = new ObservableCollection<AutoAbility>(_autoAbility.Records);

            AutoAbilityDictionary = new ObservableDictionary<ushort, string>(
                new SortedDictionary<ushort, string>(
                    _autoAbility.Records.ToDictionary(r => (ushort) (0x8000 | _autoAbility.Records.IndexOf(r)),
                        r => r.Name))
                {
                    [0] = "(None)"
                });

            _autoAbility.RecordPropertyChanged += (index, name) =>
            {
                if (name != "Name")
                    return;

                var property = _autoAbility.RecordType.GetProperty(name);
                var newValue = property?.GetValue(_autoAbility[index]) ?? string.Empty;
                AutoAbilityDictionary[(ushort) (0x8000 | index)] = (string) newValue;
            };
        }

        private static void LoadCustomizations()
        {
            _customizations = new KernelFile<Kaizou>(FFXData.Files.GetStream(CustomizationFile));

            Customization = new ObservableCollection<Kaizou>(_customizations.Records);

            CustomizationDictionary = new ObservableDictionary<ushort, string>(
                Customization.ToDictionary(r => (ushort) _customizations.IndexOf(r),
                    r => AutoAbilityDictionary.ContainsKey(r.AutoAbilityId)
                        ? AutoAbilityDictionary[r.AutoAbilityId]
                        : "(Unknown)"));

            _customizations.RecordPropertyChanged += (index, name) =>
            {
                if (name != "AutoAbilityID")
                    return;

                var property = _customizations.RecordType.GetProperty(name);
                var newValue = property?.GetValue(_customizations[index]) ?? string.Empty;
                if (AutoAbilityDictionary.ContainsKey((ushort) newValue))
                {
                    CustomizationDictionary[(ushort) index] = AutoAbilityDictionary[(ushort) newValue];
                }
                else
                {
                    CustomizationDictionary[(ushort) index] = "(Unknown)";
                }
            };
        }

        private static void LoadMonster()
        {
            _monster1 = new KernelFile<Monster>(FFXData.Files.GetStream(Monster1File));
            _monster2 = new KernelFile<Monster>(FFXData.Files.GetStream(Monster2File));
            _monster3 = new KernelFile<Monster>(FFXData.Files.GetStream(Monster3File));

            Monster = new ObservableCollection<Monster>(
                _monster1.Records.Concat(_monster2.Records.Concat(_monster3.Records)));

            // TODO: Find actual kernel page number for monster
            var m1Dic = _monster1.Records.ToDictionary(m => (ushort)(0xA000 | _monster1.IndexOf(m)), m => m.Name);
            var m2Dic = _monster2.Records.ToDictionary(m => (ushort)(0xB000 | _monster2.IndexOf(m)), m => m.Name);
            var m3Dic = _monster3.Records.ToDictionary(m => (ushort)(0xC000 | _monster3.IndexOf(m)), m => m.Name);

            MonsterDictionary = new ObservableDictionary<ushort, string>(
                new SortedDictionary<ushort, string>(
                    m1Dic.Concat(m2Dic.Concat(m3Dic)).ToDictionary(d => d.Key, d => d.Value)));

            _monster1.RecordPropertyChanged += (index, name) =>
            {
                if (name != "Name")
                    return;

                var property = _monster1.RecordType.GetProperty(name);
                var newValue = property?.GetValue(_monster1[index]) ?? string.Empty;
                MonsterDictionary[(ushort)(0xA000 | index)] = (string)newValue;
            };

            _monster2.RecordPropertyChanged += (index, name) =>
            {
                if (name != "Name")
                    return;

                var property = _monster2.RecordType.GetProperty(name);
                var newValue = property?.GetValue(_monster2[index]) ?? string.Empty;
                MonsterDictionary[(ushort)(0xB000 | index)] = (string)newValue;
            };

            _monster3.RecordPropertyChanged += (index, name) =>
            {
                if (name != "Name")
                    return;

                var property = _monster3.RecordType.GetProperty(name);
                var newValue = property?.GetValue(_monster3[index]) ?? string.Empty;
                MonsterDictionary[(ushort)(0xC000 | index)] = (string)newValue;
            };
        }

        private static void LoadSpheres()
        {
            // Load sphere file
            _spheres = new KernelFile<Sphere>(FFXData.Files.GetStream(SphereFile));

            // Create collections for user interface
            Spheres = new ObservableCollection<Sphere>(_spheres.Records);

            // Create dictionary of sphere IDs with item names
            RebuildSphereDictionary();
        }

        private static void RebuildSphereDictionary()
        {
            var sphereDict = new Dictionary<ushort, string>();
            for (ushort i = 0; i < Spheres.Count; i++)
            {
                var sphereItem = _items.Records.SingleOrDefault(r => r.SphereType == i);
                if (sphereItem == null)
                    sphereDict[i] = "Unknown";
                else
                    sphereDict[i] = sphereItem.Name;
            }

            SphereDictionary = new ObservableDictionary<ushort, string>(sphereDict);
        }

        private static void LoadNodes()
        {
            _nodes = new KernelFile<Panel>(FFXData.Files.GetStream(PanelFile));
            Nodes = new ObservableCollection<Panel>(_nodes.Records);
            NodeDictionary =
                new ObservableDictionary<ushort, string>(new SortedDictionary<ushort, string>(
                    _nodes.Records.ToDictionary(r => (ushort) _nodes.IndexOf(r),
                        r => r.Name)) {[0xFFFF] = "(None)"});

            _spheres.RecordPropertyChanged += (index, name) =>
            {
                if (name != "Name")
                    return;

                var property = _nodes.RecordType.GetProperty(name);
                var newValue = property?.GetValue(_nodes[index]) ?? string.Empty;
                NodeDictionary[(ushort) index] = (string) newValue;
            };
        }

        private static void LoadPlySave()
        {
            _plySave = new KernelFile<PlySave>(FFXData.Files.GetStream(PlySaveFile));
        }

        private static void LoadPlyRom()
        {
            _plyRom = new KernelFile<PlyRom>(FFXData.Files.GetStream(PlyRomFile));
        }

        private static void LoadSaveTxt()
        {
            _saveTxt = new KernelFile<SaveTxt>(FFXData.Files.GetStream(SaveTxtFile));
        }

        #endregion

        #region Unloaders

        private static void UnloadItems()
        {
            _items = null;
            Items = null;
            ItemDictionary = null;
        }

        private static void UnloadCommands()
        {
            _commands = null;
            Commands = null;
            CommandDictionary = null;
        }

        private static void UnloadSpheres()
        {
            _spheres = null;
            Spheres = null;
            SphereDictionary = null;
        }

        private static void UnloadNodes()
        {
            _nodes = null;
            Nodes = null;
            NodeDictionary = null;
        }

        #endregion

        #region Writers

        public static void SaveItems()
        {
            if (_items.Records.All(r => !r.IsDirty))
                return;

            var bytes = _items.GetBytes();
            FFXData.Files.WriteFile(ItemFile + ".test", bytes);
        }

        #endregion

        public static byte[] GetItemBytes() => _items.GetBytes();
        public static byte[] GetCommandBytes() => _commands.GetBytes();
        public static byte[] GetMonMagicBytes() => _monMagic1.GetBytes();
        public static byte[] GetMonMagic2Bytes() => _monMagic2.GetBytes();
        public static byte[] GetAutoAbilityBytes() => _autoAbility.GetBytes();
        public static byte[] GetCustomizationBytes() => _customizations.GetBytes();

        public static void ReplaceCommand(Command original, Command replacement)
        {
            var index = _commands.IndexOf(original);
            _commands[index] = replacement;
            Commands[index] = replacement;
            CommandDictionary[(ushort) (0x2000 | index)] = replacement.ToString();
            replacement.PropertyChanged += _commands.Record_OnPropertyChanged;
        }
    }
}