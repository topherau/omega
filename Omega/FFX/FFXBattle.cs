﻿using System.Collections.Generic;
using System.Windows;
using FFXLib.Classes;
using FFXLib.FileSystem.Data.Files.FFX;
using FFXLib.FileSystem.Data.Records.FFX.Battle;
using FFXLib.FileSystem.Data.Records.FFX.Kernel;

namespace Omega.FFX
{
    public static class FFXBattle
    {
        private const int MonsterCount = 361;
        private const string MonsterFile = "ffx_ps2/ffx/master/jppc/battle/mon/_m$MON$/m$MON$.bin";

        private static readonly Dictionary<ushort, MonsterFile> _monsterFiles = new Dictionary<ushort, MonsterFile>();

        public static ObservableDictionary<ushort, Monster> Monster { get; private set; }
        public static ObservableDictionary<ushort, MonsterLoot> MonsterLoot { get; private set; }
        public static ObservableDictionary<ushort, byte[]> MonsterScripts { get; private set; }

        public static void Load()
        {
            LoadMonsters();
        }

        public static void Save()
        {
            SaveMonsters();
        }

        public static void LoadMonsters()
        {
            var monsterDic = new Dictionary<ushort, Monster>();
            var lootDic = new Dictionary<ushort, MonsterLoot>();
            var scriptDic = new Dictionary<ushort, byte[]>();

            for (ushort i = 0; i < MonsterCount; i++)
            {
                var fileName = MonsterFile.Replace("$MON$", i.ToString("D3"));
                var fileStream = FFXData.Files.GetStream(fileName);
                _monsterFiles[i] = new MonsterFile(fileStream);
                monsterDic[i] = _monsterFiles[i].GetRecord<Monster>(MonsterFileSegment.Monster);
                lootDic[i] = _monsterFiles[i].GetRecord<MonsterLoot>(MonsterFileSegment.Loot);
                scriptDic[i] = _monsterFiles[i].GetSegment(0);
            }

            Monster = new ObservableDictionary<ushort, Monster>(monsterDic);
            MonsterLoot = new ObservableDictionary<ushort, MonsterLoot>(lootDic);
            MonsterScripts = new ObservableDictionary<ushort, byte[]>(scriptDic);
        }

        public static void SaveMonsters()
        {
            foreach (var mon in _monsterFiles)
            {
                if(Monster[mon.Key].IsDirty)
                    mon.Value.SetRecord(MonsterFileSegment.Monster, Monster[mon.Key]);
                if(MonsterLoot[mon.Key].IsDirty)
                    mon.Value.SetRecord(MonsterFileSegment.Loot, MonsterLoot[mon.Key]);
                if (Monster[mon.Key].IsDirty || MonsterLoot[mon.Key].IsDirty)
                {
                    var fileName = MonsterFile.Replace("$MON$", mon.Key.ToString("D3"));
                    var fileBytes = mon.Value.GetBytes();
                    FFXData.Files.WriteFile(fileName, fileBytes);
                }
            }
        }
    }
}