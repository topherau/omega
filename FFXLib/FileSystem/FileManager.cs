﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using FFXLib.Enums;
using FFXLib.FileSystem.VBF;

namespace FFXLib.FileSystem
{
    public class FileManager
    {
        public StringConverter StringConverter { get; private set; }

        private readonly Localization _localization;
        private readonly string _gameFile;
        private readonly string _workingPath;

        private VbfReader _vbf;
        private string _localizationString;

        public FileManager(Localization localization, string gameFile, string workingPath)
        {
            _localization = localization;
            _gameFile = gameFile;
            _workingPath = workingPath;

            Initialize();
        }

        private void Initialize()
        {
            _vbf = new VbfReader(_gameFile);

            switch (_localization)
            {
                case Localization.English:
                    _localizationString = "new_uspc";
                    break;
                case Localization.French:
                    _localizationString = "new_frpc";
                    break;
                case Localization.German:
                    _localizationString = "new_depc";
                    break;
                case Localization.Italian:
                    _localizationString = "new_itpc";
                    break;
                case Localization.Spanish:
                    _localizationString = "new_sppc";
                    break;
                case Localization.Japanese:
                    _localizationString = "new_jppc";
                    break;
                case Localization.Chinese:
                    _localizationString = "new_chpc";
                    break;
                case Localization.Korean:
                    _localizationString = "new_krpc";
                    break;
                default:
                    throw new Exception("Invalid localization specified.");
            }

            InitializeStringConverter();
        }

        private void InitializeStringConverter()
        {
            Stream stream;
            switch (_localization)
            {
                case Localization.English:
                case Localization.French:
                case Localization.German:
                case Localization.Italian:
                case Localization.Spanish:
                    stream = GetStream("ffx_ps2/ffx/master/jppc/ffx_encoding/ffxsjistbl_us.bin");
                    break;
                case Localization.Japanese:
                    stream = GetStream("ffx_ps2/ffx/master/jppc/ffx_encoding/ffxsjistbl_jp.bin");
                    break;
                case Localization.Chinese:
                    stream = GetStream("ffx_ps2/ffx/master/jppc/ffx_encoding/ffxsjistbl_cn.bin");
                    break;
                case Localization.Korean:
                    stream = GetStream("ffx_ps2/ffx/master/jppc/ffx_encoding/ffxsjistbl_kr.bin");
                    break;
                default:
                    throw new Exception("Invalid localization specified.");
            }

            var table = new List<char>();
            var reader = new BinaryReader(stream, Encoding.UTF8);
            while (stream.Position < stream.Length)
            {
                table.Add(reader.ReadChar());
            }

            StringConverter = new StringConverter(table);
        }

        public string GetLangPath(string path)
        {
            return path.Replace("$LANG$", _localizationString);
        }

        public MemoryStream GetStream(string file)
        {
            var path = GetLangPath(file);
            if (HasWorkingFile(file))
            {
                return new MemoryStream(File.ReadAllBytes(Path.Combine(_workingPath, path)));
            }
            else
            {
                return _vbf.ExtractStream(path);
            }
        }

        public void WriteFile(string file, byte[] bytes)
        {
            var path = Path.Combine(_workingPath, GetLangPath(file));
            var directory = Path.GetDirectoryName(path);
            if (directory == null)
                throw new Exception("Directory was null.");

            if (!Directory.Exists(directory))
                Directory.CreateDirectory(directory);

            File.WriteAllBytes(path, bytes);
        }

        public bool HasWorkingFile(string file)
        {
            return File.Exists(Path.Combine(_workingPath, GetLangPath(file)));
        }
    }
}