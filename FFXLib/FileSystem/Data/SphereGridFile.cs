﻿using System.Collections.Generic;
using System.IO;
using System.Reflection;
using FFXLib.Extension;
using FFXLib.FileSystem.Data.Records;
using FFXLib.FileSystem.Data.Records.FFX.SphereGrid;

namespace FFXLib.FileSystem.Data
{
    public class SphereGridFile
    {
        public List<Node> Nodes { get; }
        public List<Line> Lines { get; }
        public List<Glyph> Glyphs { get; }

        private ushort _n1;
        private ushort _u1;
        private ushort _u2;
        private ushort _u3;
        private ushort _u4;

        public SphereGridFile(MemoryStream stream)
        {
            _n1 = stream.ReadUInt16();
            var glyphCount = stream.ReadUInt16();
            var nodeCount = stream.ReadUInt16();
            var lineCount = stream.ReadUInt16();

            _u1 = stream.ReadUInt16();
            _u2 = stream.ReadUInt16();
            _u3 = stream.ReadUInt16();
            _u4 = stream.ReadUInt16();

            var glyphLength = typeof(Glyph).GetCustomAttribute<RecordLengthAttribute>();
            Glyphs = new List<Glyph>();
            for (var i = 0; i < glyphCount; i++)
            {
                var bytes = stream.ReadBytes(glyphLength.RecordLength);
                var record = Record.Parse<Glyph>(bytes);
                Glyphs.Add(record);
            }

            var nodeLength = typeof(Node).GetCustomAttribute<RecordLengthAttribute>();
            Nodes = new List<Node>();
            for (var i = 0; i < nodeCount; i++)
            {
                var bytes = stream.ReadBytes(nodeLength.RecordLength);
                var record = Record.Parse<Node>(bytes);
                Nodes.Add(record);
            }

            var lineLength = typeof(Line).GetCustomAttribute<RecordLengthAttribute>();
            Lines = new List<Line>();
            for (var i = 0; i < lineCount; i++)
            {
                var bytes = stream.ReadBytes(lineLength.RecordLength);
                var record = Record.Parse<Line>(bytes);
                Lines.Add(record);
            }
        }
    }
}