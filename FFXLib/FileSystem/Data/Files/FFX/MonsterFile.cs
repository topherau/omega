﻿using System;
using System.IO;
using FFXLib.Extension;
using FFXLib.FileSystem.Data.Records;

namespace FFXLib.FileSystem.Data.Files.FFX
{
    public class MonsterFile
    {
        private readonly Stream _stream;
        private uint _segmentCount;
        private uint[] _segmentStarts;
        private byte[][] _segmentBytes;
        private uint[] _segmentLengths;

        public MonsterFile(Stream stream)
        {
            _stream = stream;
            Read();
        }

        private void Read()
        {
            _stream.Seek(0, SeekOrigin.Begin);
            _segmentCount = _stream.ReadUInt32();
            _segmentStarts = new uint[_segmentCount];
            _segmentLengths = new uint[_segmentCount];
            _segmentBytes = new byte[_segmentCount][];

            // Read segment start offsets from file
            for (var i = 0; i < _segmentCount; i++)
            {
                _segmentStarts[i] = _stream.ReadUInt32();
            }

            // Calculate segment lengths
            for (var i = 0; i < _segmentCount; i++)
            {
                if (_segmentStarts[i] == 0)
                {
                    // Zero-length segment
                    _segmentLengths[i] = 0;
                    continue;
                }

                if (i == _segmentCount - 1)
                {
                    // Last segment in file
                    _segmentLengths[i] = (uint) _stream.Length - _segmentStarts[i];
                }
                else
                {
                    // Attempt to find next segment start that is not 0
                    var nextIndex = i + 1;
                    var nextStart = 0U;
                    while (nextIndex < _segmentCount && nextStart == 0)
                    {
                        nextStart = _segmentStarts[nextIndex];
                        nextIndex++;
                    }

                    if (nextStart == 0)
                    {
                        // No segment start found, segment length is remainder of file
                        _segmentLengths[i] = (uint) _stream.Length - _segmentStarts[i];
                    }
                    else
                    {
                        // Segment length calculated from next segment start
                        _segmentLengths[i] = nextStart - _segmentStarts[i];
                    }
                }
            }

            // Read segments from file
            for (var i = 0; i < _segmentCount; i++)
            {
                _stream.Seek(_segmentStarts[i], SeekOrigin.Begin);
                _segmentBytes[i] = _stream.ReadBytes((int) _segmentLengths[i]);
            }
        }

        public TRecord GetRecord<TRecord>(MonsterFileSegment segment) where TRecord : Record, new() =>
            GetRecord<TRecord>((int) segment);

        public TRecord GetRecord<TRecord>(int segmentId) where TRecord : Record, new()
        {
            return Record.Parse<TRecord>(_segmentBytes[segmentId]);
        }

        public void SetRecord<TRecord>(MonsterFileSegment segment, TRecord record) where TRecord : Record, new() =>
            SetRecord((int)segment, record);

        public void SetRecord<TRecord>(int segmentId, TRecord record) where TRecord : Record, new()
        {
            // Convert record to byte array
            var recordBytes = record.GetBytes();

            // Create new segment of same length and copy record bytes
            _segmentBytes[segmentId] = new byte[_segmentBytes[segmentId].Length];
            Array.Copy(recordBytes, _segmentBytes[segmentId], recordBytes.Length);
        }

        public byte[] GetSegment(int segmentId) => _segmentBytes[segmentId];
        public void SetSegment(int segmentId, byte[] bytes)
        {
            _segmentBytes[segmentId] = bytes;
            _segmentLengths[segmentId] = (uint)bytes.Length;

        }

        public byte[] GetBytes()
        {
            var stream = new MemoryStream();

            stream.Write(_segmentCount);
            foreach (var segment in _segmentStarts)
            {
                stream.Write(segment);
            }

            stream.Seek(_segmentStarts[0], SeekOrigin.Begin);
            foreach (var segment in _segmentBytes)
            {
                stream.WriteBytes(segment);
            }

            return stream.ToArray();
        }
    }

    public enum MonsterFileSegment
    {
        Script,
        Unknown1,
        Monster,
        Unknown2,
        Loot,
        Unknown3,
        Unknown4,
        Unknown5
    }
}