﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using FFXLib.Extension;
using FFXLib.FileSystem.Data.Records;

namespace FFXLib.FileSystem.Data.Files
{
    public class StringTableFile
    {
        public ObservableCollection<string> Strings { get; set; }

        public StringTableFile(MemoryStream stream)
        {
            Read(stream);
        }

        private void Read(MemoryStream stream)
        {
            var offsets = new List<uint>();
            do
            {
                offsets.Add(stream.ReadUInt16());
                stream.ReadUInt16();
            } while (stream.Position < offsets[0]);

            var strings = new Dictionary<uint, string>();
            foreach (var offset in offsets)
            {
                if (strings.ContainsKey(offset))
                    continue;

                stream.Seek(offset, SeekOrigin.Begin);
                var stringBytes = new List<byte>();
                while(stream.Position < stream.Length)
                {
                    var b = stream.ReadByte();
                    stringBytes.Add((byte)b);
                    if (b == 0)
                        break;
                }
                
                strings[offset] = Record.StringConverter.Decode(stringBytes.ToArray(), 0);
            }

            Strings = new ObservableCollection<string>(strings.Select(s => s.Value));
        }
    }
}