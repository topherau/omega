﻿using System;

namespace FFXLib.FileSystem.Data.Records
{
    [AttributeUsage(AttributeTargets.Class)]
    public class RecordLengthAttribute : Attribute
    {
        public int RecordLength { get; }

        public RecordLengthAttribute(int recordLength)
        {
            RecordLength = recordLength;
        }
    }
}