﻿using System;

namespace FFXLib.FileSystem.Data.Records
{
    /// <summary>
    /// Specifies that the property should be handled when reading or writing this record.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class RecordDataAttribute : Attribute
    {
        public int Offset { get; private set; }
        public int ArrayLength { get; private set; }

        /// <summary>
        /// Specifies that the property should be processed when reading or writing this record type.
        /// </summary>
        /// <param name="offset">The zero-based offset of the property in the data array.</param>
        public RecordDataAttribute(int offset)
        {
            Offset = offset;
            ArrayLength = 1;
        }

        /// <summary>
        /// Specifies that the property should be processed as an array when reading or writing this record type.
        /// </summary>
        /// <param name="offset">The zero-based offset of the property in the data array.</param>
        /// <param name="arrayLength">The length of the array.</param>
        public RecordDataAttribute(int offset, int arrayLength)
        {
            Offset = offset;
            ArrayLength = arrayLength;
        }
    }
}