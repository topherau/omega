﻿//using System;
//using System.Collections;
//using System.Collections.Generic;
//using System.Collections.ObjectModel;
//using System.Collections.Specialized;
//using System.ComponentModel;
//using System.IO;
//using System.Linq;
//using System.Reflection;
//using System.Runtime.CompilerServices;
//using System.Runtime.InteropServices;
//using FFXLib.Extension;
//using Newtonsoft.Json;

//namespace FFXLib.FileSystem.Data.Records
//{
//    public class OldRecord : INotifyPropertyChanged
//    {
//        public event PropertyChangedEventHandler PropertyChanged;

//        [JsonIgnore]
//        public bool IsDirty { get; set; }

//        [JsonIgnore]
//        public byte[] Data { get; set; }

//        private bool _readFinished;
//        private static StringConverter _stringConverter;

//        public static void LoadStringConverter(StringConverter converter)
//        {
//            _stringConverter = converter;
//        }

//        public void SetStringOffset(PropertyInfo stringProperty, ushort offset)
//        {
//            if (stringProperty.PropertyType != typeof(string))
//                return;

//            var attribute = stringProperty.GetCustomAttribute<RecordDataAttribute>();
//            if (attribute == null)
//                return;

//            Array.Copy(BitConverter.GetBytes(offset), 0, Data, attribute.Offset, 2);
//        }

//        public static TRecord Parse<TRecord>(byte[] inputBytes, byte[] stringTable = null) where TRecord : Record
//        {
//            // Retrieve all properties with RecordDataAttribute
//            var properties = typeof(TRecord).GetProperties()
//                .Where(p => p.GetCustomAttribute<RecordDataAttribute>() != null).ToList();
//            if (properties.Count == 0)
//                throw new Exception($"{typeof(TRecord)} does not contain any properties with RecordDataAttribute!");

//            var stream = new MemoryStream(inputBytes);
//            var record = (TRecord) Activator.CreateInstance(typeof(TRecord));
            
//            foreach (var property in properties)
//            {
//                var attribute = property.GetCustomAttribute<RecordDataAttribute>();
//                var propertyType = property.PropertyType;
//                var propertyLength = 1;

//                // For collection types, get the element type from the generic type parameter and array count from attribute
//                if (propertyType.GetInterface("ICollection") != null)
//                {
//                    if (propertyType.GenericTypeArguments.Length != 1)
//                        continue;

//                    propertyType = propertyType.GenericTypeArguments[0];
//                    propertyLength = attribute.ArrayLength;
//                }

//                // For enum types, we want to read the underlying type then convert to enum
//                if (propertyType.IsEnum)
//                    propertyType = Enum.GetUnderlyingType(propertyType);

//                // Create array of property type to read values into
//                var propertyValue = Array.CreateInstance(propertyType, propertyLength);
//                stream.Seek(attribute.Offset, SeekOrigin.Begin);

//                // Read values
//                for (var i = 0; i < propertyLength; i++)
//                {
//                    if (propertyType == typeof(ulong))
//                    {
//                        propertyValue.SetValue(stream.ReadUInt64(), i);
//                    }
//                    else if (propertyType == typeof(uint))
//                    {
//                        propertyValue.SetValue(stream.ReadUInt32(), i);
//                    }
//                    else if (propertyType == typeof(ushort))
//                    {
//                        propertyValue.SetValue(stream.ReadUInt16(), i);
//                    }
//                    else if (propertyType == typeof(byte))
//                    {
//                        propertyValue.SetValue(stream.ReadUByte(), i);
//                    }
//                    else if (propertyType == typeof(long))
//                    {
//                        propertyValue.SetValue(stream.ReadInt64(), i);
//                    }
//                    else if (propertyType == typeof(int))
//                    {
//                        propertyValue.SetValue(stream.ReadInt32(), i);
//                    }
//                    else if (propertyType == typeof(short))
//                    {
//                        propertyValue.SetValue(stream.ReadInt16(), i);
//                    }
//                    else if (propertyType == typeof(sbyte))
//                    {
//                        propertyValue.SetValue(stream.ReadSByte(), i);
//                    }
//                    else if (propertyType == typeof(string))
//                    {
//                        var stringPos = stream.ReadUInt16();
//                        stream.ReadUInt16(); // Advance 2 bytes for junk data
//                        if (stringTable == null || stringPos >= stringTable.Length)
//                            continue;

//                        propertyValue.SetValue(_stringConverter.Decode(stringTable, stringPos), i);
//                    }
//                }
                
//                if (propertyLength != 1)
//                {
//                    // Create collection type with value array as source
//                    var collection = Activator.CreateInstance(property.PropertyType, propertyValue);

//                    // Attach an event listener so we know when the data was changed
//                    if (property.PropertyType.GetInterface("INotifyCollectionChanged") != null)
//                        ((INotifyCollectionChanged) collection).CollectionChanged += (s, e) =>
//                        {
//                            record.OnCollectionChanged(property, e);
//                        };

//                    property.SetValue(record, collection);
//                }
//                else
//                {
//                    property.SetValue(record, propertyValue.GetValue(0));
//                }
//            }

//            record.Data = stream.ToArray();
//            record._readFinished = true;
//            return record;
//        }

//        /// <summary>
//        /// Write the value stored in the specified <paramref name="property"/> to the underlying byte array.
//        /// </summary>
//        /// <param name="property">The property to write.</param>
//        private void WriteValue(PropertyInfo property)
//        {
//            var attribute = property.GetCustomAttribute<RecordDataAttribute>();
//            if (attribute == null)
//                return;

//            var propertyType = property.PropertyType;

//            if (propertyType == typeof(string))
//            {
//                // String offsets need to be manually written later
//                Array.Copy(BitConverter.GetBytes(0), 0, Data, attribute.Offset, 4);
//                return;
//            }
                

//            var propertyLength = 1;
//            var isCollection = propertyType.GetInterface("ICollection") != null;
            
//            if (isCollection)
//            {
//                if (propertyType.GenericTypeArguments.Length != 1)
//                    return;

//                propertyType = propertyType.GenericTypeArguments[0];
//                propertyLength = attribute.ArrayLength;
//            }

//            if (propertyType.IsEnum)
//                propertyType = Enum.GetUnderlyingType(propertyType);

//            // Convert value to bytes and write to underlying data array
//            var propertySize = Marshal.SizeOf(propertyType);
//            var propertyValues = Array.CreateInstance(propertyType, propertyLength);
            
//            if(isCollection)
//            {
//                ((ICollection)property.GetValue(this)).CopyTo(propertyValues, 0);
//            }
//            else
//            {
//                propertyValues.SetValue(property.GetValue(this), 0);
//            }

//            for (var i = 0; i < propertyLength; i++)
//            {
//                var value = propertyValues.GetValue(i);
//                if (propertyType == typeof(ulong))
//                    Array.Copy(BitConverter.GetBytes((ulong)value), 0, Data,
//                        attribute.Offset + i * propertySize,
//                        propertySize);
//                else if (propertyType == typeof(uint))
//                    Array.Copy(BitConverter.GetBytes((uint)value), 0, Data,
//                        attribute.Offset + i * propertySize,
//                        propertySize);
//                else if (propertyType == typeof(ushort))
//                    Array.Copy(BitConverter.GetBytes((ushort)value), 0, Data,
//                        attribute.Offset + i * propertySize,
//                        propertySize);
//                else if (propertyType == typeof(byte))
//                    Array.Copy(BitConverter.GetBytes((byte)value), 0, Data,
//                        attribute.Offset + i * propertySize,
//                        propertySize);
//                else if (propertyType == typeof(long))
//                    Array.Copy(BitConverter.GetBytes((long)value), 0, Data,
//                        attribute.Offset + i * propertySize,
//                        propertySize);
//                else if (propertyType == typeof(int))
//                    Array.Copy(BitConverter.GetBytes((int)value), 0, Data,
//                        attribute.Offset + i * propertySize,
//                        propertySize);
//                else if (propertyType == typeof(short))
//                    Array.Copy(BitConverter.GetBytes((short)value), 0, Data,
//                        attribute.Offset + i * propertySize,
//                        propertySize);
//                else if (propertyType == typeof(sbyte))
//                    Array.Copy(BitConverter.GetBytes((sbyte)value), 0, Data,
//                        attribute.Offset + i * propertySize,
//                        propertySize);
//            }
                
//        }
        
//        protected virtual void OnPropertyChanged(string propertyName)
//        {
//            if (!_readFinished ||
//                propertyName == "IsDirty" ||
//                propertyName == "Data")
//                return;

//            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

//            IsDirty = true;


//            var property = GetType().GetProperty(propertyName);
//            var attribute = property.GetCustomAttribute<RecordDataAttribute>();
//            if (attribute == null)
//                return;

//            if (Data == null)
//                Data = new byte[0];

//            var propertyType = property.PropertyType;

//            if (propertyType == typeof(string))
//                propertyType = typeof(uint);
            
//            var propertyLength = 1;
//            var isCollection = propertyType.GetInterface("ICollection") != null;

//            if (isCollection)
//            {
//                if (propertyType.GenericTypeArguments.Length != 1)
//                    return;

//                propertyType = propertyType.GenericTypeArguments[0];
//                propertyLength = attribute.ArrayLength;
//            }

//            if (propertyType.IsEnum)
//                propertyType = Enum.GetUnderlyingType(propertyType);

//            // Convert value to bytes and write to underlying data array
//            var propertySize = Marshal.SizeOf(propertyType) * propertyLength;

//            var valueEnd = attribute.Offset + propertySize;

//            if (valueEnd > Data.Length)
//            {
//                var data = Data;
//                Array.Resize(ref data, valueEnd);
//                Data = data;
//            }

//            WriteValue(property);
//        }

//        private void OnCollectionChanged(PropertyInfo collectionInfo, NotifyCollectionChangedEventArgs args)
//        {
//            IsDirty = true;

//            // Update the value of the array in the underlying data.
//            WriteValue(collectionInfo);

//            // Invoke PropertyChanged for collection modifications
//            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(collectionInfo.Name));
//        }

//    }
//}