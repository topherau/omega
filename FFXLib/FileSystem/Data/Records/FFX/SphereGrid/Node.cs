﻿namespace FFXLib.FileSystem.Data.Records.FFX.SphereGrid
{
    [RecordLength(0x0C)]
    public class Node : Record
    {
        [RecordData(0x00)] public short X { get; set; }
        [RecordData(0x02)] public short Y { get; set; }
        [RecordData(0x04)] public ushort Unknown0004 { get; set; }
        [RecordData(0x06)] public ushort DefaultType { get; set; }
        [RecordData(0x08)] public ushort AttachedGlyph { get; set; }
        [RecordData(0x0A)] public ushort Unknown000A { get; set; }
    }
}