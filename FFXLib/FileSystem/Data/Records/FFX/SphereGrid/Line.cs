﻿namespace FFXLib.FileSystem.Data.Records.FFX.SphereGrid
{
    [RecordLength(0x08)]
    public class Line : Record
    {
        [RecordData(0x00)] public ushort Node1 { get; set; }
        [RecordData(0x02)] public ushort Node2 { get; set; }
        [RecordData(0x04)] public ushort CenterNode { get; set; }
        [RecordData(0x06)] public ushort Unknown0006 { get; set; }
    }
}