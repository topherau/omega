﻿namespace FFXLib.FileSystem.Data.Records.FFX.Kernel
{
    [RecordLength(0x08)]
    public class Kaizou : Record
    {
        [RecordData(0x00)] public ushort CustomizeType { get; set; } // 1 = Weapon, 2 = Armor

        [RecordData(0x02)] public ushort AutoAbilityId { get; set; }

        [RecordData(0x04)] public ushort ItemId { get; set; }

        [RecordData(0x06)] public ushort AmountRequired { get; set; }
    }
}