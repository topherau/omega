﻿using FFXLib.Enums.Flags;

namespace FFXLib.FileSystem.Data.Records.FFX.Kernel
{
    /// <summary>
    /// Describes the properties of each type of sphere.
    /// </summary>
    public class Sphere : Record
    {
        [RecordData(0x00)]
        public string Description { get; set; }

        [RecordData(0x04)]
        public string String1 { get; set; }

        [RecordData(0x08)]
        public ushort Unknown08 { get; set; }

        [RecordData(0x0a)]
        public SphereNodeStatFlags NodeType { get; set; }

        [RecordData(0x0c)]
        public byte ActivationType { get; set; }

        [RecordData(0x0d)]
        public byte Effect { get; set; }

        [RecordData(0x0e)]
        public ushort Unknown0E { get; set; }
    }
}