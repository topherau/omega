﻿namespace FFXLib.FileSystem.Data.Records.FFX.Kernel
{
    public class PlySave : Record
    {
        [RecordData(0x00)] public string Name { get; set; }
    }
}