﻿namespace FFXLib.FileSystem.Data.Records.FFX.Kernel
{
    public class PlyRom : Record
    {
        [RecordData(0x0000)] public string Name { get; set; }

        [RecordData(0x0004)] public string String1 { get; set; }

        [RecordData(0x0008)] public string Description { get; set; }

        [RecordData(0x000c)] public string String2 { get; set; }
    }
}