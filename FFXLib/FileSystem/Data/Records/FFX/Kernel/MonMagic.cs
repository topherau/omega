﻿using FFXLib.Enums.Flags.FFX;
using FFXLib.FileSystem.Data.Records;

namespace FFXLib.FileSystem.Data.Records.FFX.Kernel
{
    public class MonMagic : Record
    {
        [RecordData(0x0000)] public string Name { get; set; }

        [RecordData(0x0004)] public string Description { get; set; }

        [RecordData(0x0008)] public uint Unknown0008 { get; set; }

        [RecordData(0x000C)] public uint Unknown000C { get; set; }

        [RecordData(0x0010)] public ushort SpellAnimation1 { get; set; }

        [RecordData(0x0012)] public ushort SpellAnimation2 { get; set; }

        [RecordData(0x0014)] public byte Icon { get; set; }

        [RecordData(0x0015)] public byte CasterAnimation { get; set; }

        // Unknown
        [RecordData(0x0016)] public byte Unknown0016 { get; set; }

        // Unknown
        [RecordData(0x0017)] public byte Unknown0017 { get; set; }

        [RecordData(0x0018)] public byte MenuCategory { get; set; }

        // Unknown
        [RecordData(0x0019)] public byte Unknown0019 { get; set; }

        [RecordData(0x001A)] public TargetFlags TargetFlags { get; set; }

        [RecordData(0x001C)] public BattleFlags2 BattleFlags2 { get; set; }

        [RecordData(0x001D)] public BattleFlags BattleFlags { get; set; }

        [RecordData(0x0021)] public byte StealGil { get; set; }

        [RecordData(0x0022)] public byte FriendlyDisplay { get; set; }

        [RecordData(0x0023)] public DamageTypeFlags DamageType { get; set; }

        [RecordData(0x0024)] public byte Cooldown { get; set; }

        [RecordData(0x0025)] public byte MpCost { get; set; }

        [RecordData(0x0026)] public byte OverdriveCost { get; set; }

        // Unknown
        [RecordData(0x0027)] public byte IncreaseCritical { get; set; }

        [RecordData(0x0028)] public byte DamageFormula { get; set; }

        // Unknown
        [RecordData(0x0029)] public byte Accuracy { get; set; }

        [RecordData(0x002a)] public byte DamagePower { get; set; }

        [RecordData(0x002b)] public byte DamageHits { get; set; }

        // Unknown
        [RecordData(0x002c)] public byte PetrificationDestructionRate { get; set; }

        [RecordData(0x002d)] public ElementalFlags Elemental { get; set; }

        [RecordData(0x002e)] public byte KoChance { get; set; }

        [RecordData(0x002f)] public byte ZombieChance { get; set; }

        [RecordData(0x0030)] public byte PetrifyChance { get; set; }

        [RecordData(0x0031)] public byte PoisonChance { get; set; }

        [RecordData(0x0032)] public byte PowerBreakChance { get; set; }

        [RecordData(0x0033)] public byte MagicBreakChance { get; set; }

        [RecordData(0x0034)] public byte ArmorBreakChance { get; set; }

        [RecordData(0x0035)] public byte MentalBreakChance { get; set; }

        [RecordData(0x0036)] public byte ConfusionChance { get; set; }

        [RecordData(0x0037)] public byte BerserkChance { get; set; }

        [RecordData(0x0038)] public byte ProvokeChance { get; set; }

        [RecordData(0x0039)] public byte ThreatenChance { get; set; }

        [RecordData(0x003a)] public byte SleepChance { get; set; }

        [RecordData(0x003b)] public byte SilenceChance { get; set; }

        [RecordData(0x003c)] public byte DarknessChance { get; set; }

        [RecordData(0x003d)] public byte ShellChance { get; set; }

        [RecordData(0x003e)] public byte ProtectChance { get; set; }

        [RecordData(0x003f)] public byte ReflectChance { get; set; }

        [RecordData(0x0040)] public byte NulTideChance { get; set; }

        [RecordData(0x0041)] public byte NulBlazeChance { get; set; }

        [RecordData(0x0042)] public byte NulFrostChance { get; set; }

        [RecordData(0x0043)] public byte NulShockChance { get; set; }

        [RecordData(0x0044)] public byte RegenChance { get; set; }

        [RecordData(0x0045)] public byte HasteChance { get; set; }

        [RecordData(0x0046)] public byte SlowChance { get; set; }

        [RecordData(0x0047)] public byte SleepTurns { get; set; }

        [RecordData(0x0048)] public byte SilenceTurns { get; set; }

        [RecordData(0x0049)] public byte DarknessTurns { get; set; }

        [RecordData(0x004a)] public byte ShellTurns { get; set; }

        [RecordData(0x004b)] public byte ProtectTurns { get; set; }

        [RecordData(0x004c)] public byte ReflectTurns { get; set; }

        [RecordData(0x004d)] public byte NulTideTurns { get; set; }

        [RecordData(0x004e)] public byte NulBlazeTurns { get; set; }

        [RecordData(0x004f)] public byte NulFrostTurns { get; set; }

        [RecordData(0x0050)] public byte NulShockTurns { get; set; }

        [RecordData(0x0051)] public byte RegenTurns { get; set; }

        [RecordData(0x0052)] public byte HasteTurns { get; set; }

        [RecordData(0x0053)] public byte SlowTurns { get; set; }

        [RecordData(0x0054)] public StatusFlags StatusFlags { get; set; }

        [RecordData(0x0056)] public BuffFlags BuffFlags { get; set; }

        // Unknown
        [RecordData(0x0057)] public byte Unknown0057 { get; set; }

        // Unknown
        [RecordData(0x0058)] public byte Unknown0058 { get; set; }

        [RecordData(0x0059)] public byte BuffStacks { get; set; }

        [RecordData(0x005a)] public SpecialBuffFlags SpecialBuffFlags { get; set; }

        // Unknown
        [RecordData(0x005b)] public byte Unknown005B { get; set; }

        public override string ToString()
        {
            return $"{Name}";
        }
    }
}