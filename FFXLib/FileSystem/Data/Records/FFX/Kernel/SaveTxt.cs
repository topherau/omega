﻿using System.Collections.ObjectModel;

namespace FFXLib.FileSystem.Data.Records.FFX.Kernel
{
    public class SaveTxt : Record
    {
        [RecordData(0x00)] public string Name { get;set; }
        [RecordData(0x04)] public string Description { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}