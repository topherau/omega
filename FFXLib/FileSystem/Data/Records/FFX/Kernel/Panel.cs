﻿using FFXLib.Enums.Flags;

namespace FFXLib.FileSystem.Data.Records.FFX.Kernel
{
    /// <summary>
    /// Describes the properties of nodes on the Sphere Grid.
    /// </summary>
    public class Panel : Record
    {
        [RecordData(0x0000)]
        public string Name { get; set; }

        [RecordData(0x0004)]
        public string String1 { get; set; }

        [RecordData(0x0008)]
        public string Description { get; set; }

        [RecordData(0x000c)]
        public string String2 { get; set; }

        [RecordData(0x0010)]
        public SphereNodeStatFlags StatType { get; set; }

        [RecordData(0x0012)]
        public ushort AbilityLearned { get; set; }

        [RecordData(0x0014)]
        public ushort Multiplier { get; set; }

        [RecordData(0x0016)]
        public ushort Icon { get; set; }

        public override string ToString()
        {
            return $"{Name} - {StatType} {Multiplier} {AbilityLearned:X4} {Icon}";
        }
    }
}