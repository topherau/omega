﻿using FFXLib.Enums.Flags.FFX;

namespace FFXLib.FileSystem.Data.Records.FFX.Kernel
{
    public class AutoAbility : Record
    {
        [RecordData(0x0000)] public string Name { get; set; }

        [RecordData(0x0004)] public string String1 { get; set; }

        [RecordData(0x0008)] public string Description { get; set; }

        [RecordData(0x000c)] public string String2 { get; set; }

        [RecordData(0x0010)] public byte ActivationType { get; set; }

        [RecordData(0x0011)] public ElementalFlags ElementalAttack { get; set; }

        [RecordData(0x0012)] public ElementalFlags ElementalAbsorb { get; set; }

        [RecordData(0x0013)] public ElementalFlags ElementalImmune { get; set; }

        [RecordData(0x0014)] public ElementalFlags ElementalResist { get; set; }

        [RecordData(0x0015)] public byte Unused1 { get; set; }

        [RecordData(0x0016)] public byte DeathChance { get; set; }

        [RecordData(0x0017)] public byte ZombieChance { get; set; }

        [RecordData(0x0018)] public byte PetrifyChance { get; set; }

        [RecordData(0x0019)] public byte PoisonChance { get; set; }

        [RecordData(0x001a)] public byte PowerBreakChance { get; set; }

        [RecordData(0x001b)] public byte MagicBreakChance { get; set; }

        [RecordData(0x001c)] public byte ArmorBreakChance { get; set; }

        [RecordData(0x001d)] public byte MentalBreakChance { get; set; }

        [RecordData(0x001e)] public byte ConfusionChance { get; set; }

        [RecordData(0x001f)] public byte BerserkChance { get; set; }

        [RecordData(0x0020)] public byte ProvokeChance { get; set; }

        [RecordData(0x0021)] public byte ThreatenChance { get; set; }

        [RecordData(0x0022)] public byte SleepChance { get; set; }

        [RecordData(0x0023)] public byte SilenceChance { get; set; }

        [RecordData(0x0024)] public byte DarkChance { get; set; }

        [RecordData(0x0025)] public byte ShellChance { get; set; }

        [RecordData(0x0026)] public byte ProtectChance { get; set; }

        [RecordData(0x0027)] public byte ReflectChance { get; set; }

        [RecordData(0x0028)] public byte NulTideChance { get; set; }

        [RecordData(0x0029)] public byte NulBlazeChance { get; set; }

        [RecordData(0x002a)] public byte NulFrostChance { get; set; }

        [RecordData(0x002b)] public byte NulShockChance { get; set; }

        [RecordData(0x002c)] public byte RegenChance { get; set; }

        [RecordData(0x002d)] public byte HasteChance { get; set; }

        [RecordData(0x002e)] public byte SlowChance { get; set; }

        [RecordData(0x002f)] public byte SleepTurns { get; set; }

        [RecordData(0x0030)] public byte SilenceTurns { get; set; }

        [RecordData(0x0031)] public byte DarkTurns { get; set; }

        [RecordData(0x0032)] public byte ShellTurns { get; set; }

        [RecordData(0x0033)] public byte ProtectTurns { get; set; }

        [RecordData(0x0034)] public byte ReflectTurns { get; set; }

        [RecordData(0x0035)] public byte NulTideTurns { get; set; }

        [RecordData(0x0036)] public byte NulBlazeTurns { get; set; }

        [RecordData(0x0037)] public byte NulFrostTurns { get; set; }

        [RecordData(0x0038)] public byte NulShockTurns { get; set; }

        [RecordData(0x0039)] public byte RegenTurns { get; set; }

        [RecordData(0x003a)] public byte HasteTurns { get; set; }

        [RecordData(0x003b)] public byte SlowTurns { get; set; }

        [RecordData(0x003c)] public byte DeathResist { get; set; }

        [RecordData(0x003d)] public byte ZombieResist { get; set; }

        [RecordData(0x003e)] public byte PetrifyResist { get; set; }

        [RecordData(0x003f)] public byte PoisonResist { get; set; }

        [RecordData(0x0040)] public byte PowerBreakResist { get; set; }

        [RecordData(0x0041)] public byte MagicBreakResist { get; set; }

        [RecordData(0x0042)] public byte ArmorBreakResist { get; set; }

        [RecordData(0x0043)] public byte MentalBreakResist { get; set; }

        [RecordData(0x0044)] public byte ConfusionResist { get; set; }

        [RecordData(0x0045)] public byte BerserkResist { get; set; }

        [RecordData(0x0046)] public byte ProvokeResist { get; set; }

        [RecordData(0x0047)] public byte ThreatenResist { get; set; }

        [RecordData(0x0048)] public byte SleepResist { get; set; }

        [RecordData(0x0049)] public byte SilenceResist { get; set; }

        [RecordData(0x004a)] public byte DarknessResist { get; set; }

        [RecordData(0x004b)] public byte ShellResist { get; set; }

        [RecordData(0x004c)] public byte ProtectResist { get; set; }

        [RecordData(0x004d)] public byte ReflectResist { get; set; }

        [RecordData(0x004e)] public byte NulTideResist { get; set; }

        [RecordData(0x004f)] public byte NulBlazeResist { get; set; }

        [RecordData(0x0050)] public byte NulFrostResist { get; set; }

        [RecordData(0x0051)] public byte NulShockResist { get; set; }

        [RecordData(0x0052)] public byte RegenResist { get; set; }

        [RecordData(0x0053)] public byte HasteResist { get; set; }

        [RecordData(0x0054)] public byte SlowResist { get; set; }

        [RecordData(0x0055)] public byte StatIncreasePercent { get; set; }

        [RecordData(0x0056)] public StatIncreaseFlags StatIncreaseType { get; set; }

        [RecordData(0x0058)] public PermanentStatusFlags PermanentStatus { get; set; }

        [RecordData(0x005c)] public StatusFlags StatusAuto { get; set; }

        [RecordData(0x005e)] public StatusFlags StatusInflict { get; set; }

        [RecordData(0x0060)] public StatusFlags StatusImmune { get; set; }

        [RecordData(0x0062)] public SpecialEffectFlags1 SpecialEffect1 { get; set; }

        [RecordData(0x0066)] public SpecialEffectFlags2 SpecialEffect2 { get; set; }

        [RecordData(0x0067)] public byte Unused3 { get; set; }

        [RecordData(0x0068)] public byte Icon { get; set; }

        [RecordData(0x0069)] public byte LockoutGroup { get; set; }

        [RecordData(0x006a)] public byte LockoutLevel { get; set; }

        [RecordData(0x006b)] public byte LockoutGlobal { get; set; }
    }
}