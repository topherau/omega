﻿using System.Collections.ObjectModel;

namespace FFXLib.FileSystem.Data.Records.FFX.Battle
{
    public class MonsterLoot : Record
    {
        [RecordData(0x0000)] public ushort Gil { get; set; }

        [RecordData(0x0002)] public ushort Ap { get; set; }

        [RecordData(0x0004)] public ushort ApOverkill { get; set; }

        [RecordData(0x0006)] public byte Unknown0006 { get; set; }

        [RecordData(0x0007)] public byte Unknown0007 { get; set; }

        [RecordData(0x0008)] public byte Unknown0008 { get; set; }

        [RecordData(0x0009)] public byte Unknown0009 { get; set; }

        [RecordData(0x000a)] public ushort EquipmentDropRate { get; set; }

        [RecordData(0x000c)] public ushort CommonDrop1 { get; set; }

        [RecordData(0x000e)] public ushort CommonDrop2 { get; set; }

        [RecordData(0x0010)] public ushort CommonDrop3 { get; set; }

        [RecordData(0x0012)] public ushort CommonDrop4 { get; set; }

        [RecordData(0x0014)] public byte CommonDrop1Amount { get; set; }

        [RecordData(0x0015)] public byte CommonDrop2Amount { get; set; }

        [RecordData(0x0016)] public byte CommonDrop3Amount { get; set; }

        [RecordData(0x0017)] public byte CommonDrop4Amount { get; set; }

        [RecordData(0x0018)] public ushort RareDrop1 { get; set; }

        [RecordData(0x001a)] public ushort RareDrop2 { get; set; }

        [RecordData(0x001c)] public ushort RareDrop3 { get; set; }

        [RecordData(0x001e)] public ushort RareDrop4 { get; set; }

        [RecordData(0x0020)] public byte RareDrop1Amount { get; set; }

        [RecordData(0x0021)] public byte RareDrop2Amount { get; set; }

        [RecordData(0x0022)] public byte RareDrop3Amount { get; set; }

        [RecordData(0x0023)] public byte RareDrop4Amount { get; set; }

        [RecordData(0x0024)] public ushort CommonSteal { get; set; }

        [RecordData(0x0026)] public ushort RareSteal { get; set; }

        [RecordData(0x0028)] public byte CommonStealAmount { get; set; }

        [RecordData(0x0029)] public byte RareStealAmount { get; set; }

        [RecordData(0x002a)] public ushort Bribe { get; set; }

        [RecordData(0x002c)] public byte BribeAmount { get; set; }

        [RecordData(0x002d)] public byte Unknown002D { get; set; }

        [RecordData(0x002e)] public byte Unknown002E { get; set; }

        [RecordData(0x002f)] public byte CriticalBonus { get; set; }

        [RecordData(0x0030)] public byte Unknown0030 { get; set; }

        [RecordData(0x0031)] public byte Unknown0031 { get; set; }

        [RecordData(0x0032, 16)] public ObservableCollection<ushort> TidusAbility { get; set; }

        [RecordData(0x0052, 16)] public ObservableCollection<ushort> YunaAbility { get; set; }

        [RecordData(0x0072, 16)] public ObservableCollection<ushort> AuronAbility { get; set; }

        [RecordData(0x0092, 16)] public ObservableCollection<ushort> KimahriAbility { get; set; }

        [RecordData(0x00b2, 16)] public ObservableCollection<ushort> WakkaAbility { get; set; }

        [RecordData(0x00d2, 16)] public ObservableCollection<ushort> LuluAbility { get; set; }

        [RecordData(0x00f2, 16)] public ObservableCollection<ushort> RikkuAbility { get; set; }

        [RecordData(0x0112)] public byte Unknown0112 { get; set; }

        [RecordData(0x0113)] public byte Unknown0113 { get; set; }

        [RecordData(0x0114)] public ushort MonsterArenaPrice { get; set; }
        
        [RecordData(0x0116)] public byte Unknown0116 { get; set; }
        
        [RecordData(0x0117)] public byte Unknown0117 { get; set; }
    }
}