﻿using System;
using System.ComponentModel;
using System.Configuration;
using System.IO;
using Newtonsoft.Json;

namespace FFXLib.Configuration
{
    public class ConfigFile<TConfig> where TConfig : INotifyPropertyChanged
    {
        public TConfig Configuration { get; private set; }

        private readonly string _configPath;

        public ConfigFile(string configPath, bool createNew = false)
        {
            _configPath = configPath;

            Load(createNew);
        }

        public void Load(bool createNew = false)
        {
            if (createNew && !File.Exists(_configPath))
                CreateDefaultConfig();

            try
            {
                var configJson = File.ReadAllText(_configPath);
                Configuration = JsonConvert.DeserializeObject<TConfig>(configJson);
                Configuration.PropertyChanged += (sender, args) => Save();

            }
            catch (Exception ex)
            {
                throw new ConfigurationErrorsException($"Failed to read {_configPath}, see inner exception for more details.", ex);
            }
        }

        public void Save()
        {
            try
            {
                var configJson = JsonConvert.SerializeObject(Configuration, Formatting.Indented);
                File.WriteAllText(_configPath, configJson);
            }
            catch (Exception ex)
            {
                throw new ConfigurationErrorsException($"Failed to write {_configPath}, see inner exception for more details.", ex);
            }
        }

        private void CreateDefaultConfig()
        {
            try
            {
                var config = Activator.CreateInstance(typeof(TConfig));
                var configJson = JsonConvert.SerializeObject(config, Formatting.Indented);
                File.WriteAllText(_configPath, configJson);
            }
            catch (Exception ex)
            {
                throw new ConfigurationErrorsException(
                    $"Failed to create {_configPath}, see inner exception for more details.", ex);
            }
            
            
        }
    }
}