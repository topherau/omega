﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace FFXLib.Classes
{
    public class JsonFlagConverter<TFlag> : JsonConverter<TFlag> where TFlag : IConvertible
    {
        public override void WriteJson(JsonWriter writer, TFlag value, JsonSerializer serializer)
        {
            var flagsType = value.GetType();
            var flagsList = new List<string>();

            var longValue = value.ToUInt64(CultureInfo.CurrentCulture);
            
            foreach (var enumValue in Enum.GetValues(flagsType))
            {
                var longEnumVal = (ulong)Convert.ChangeType(enumValue, typeof(ulong));
                if ((longValue & longEnumVal) == 0)
                    continue;
                flagsList.Add($"\"{Enum.GetName(value.GetType(), enumValue)}\"");
            }

            writer.WriteRawValue($"[{string.Join(", ", flagsList)}]");
        }

        public override TFlag ReadJson(JsonReader reader, Type objectType, TFlag existingValue, bool hasExistingValue, JsonSerializer serializer)
        {
            var outputValue = 0UL;

            while (reader.Read() && reader.Value != null)
            {
                var flagValue = (string) reader.Value;

                try
                {
                    var enumValue = Enum.Parse(typeof(TFlag), flagValue);
                    var longValue = (ulong) Convert.ChangeType(enumValue, typeof(ulong));
                    outputValue |= longValue;
                }
                catch (Exception)
                {
                    //ignored
                }
            }
            

            var r = (TFlag)Enum.ToObject(objectType, outputValue);
            return r;
        }
    }
}