﻿namespace FFXLib.Enums
{
    public enum Localization
    {
        English,
        French,
        German,
        Italian,
        Spanish,
        Japanese,
        Chinese,
        Korean
    }
}