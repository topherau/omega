﻿using System;
using FFXLib.Classes;
using Newtonsoft.Json;

namespace FFXLib.Enums.Flags.FFX
{
    [Flags]
    [JsonConverter(typeof(JsonFlagConverter<DamageTypeFlags>))]
    public enum DamageTypeFlags : byte
    {
        HP = 0x01,
        MP = 0x02,
        Speed = 0x04
    }
}