﻿namespace FFXLib.Enums.Flags.FFX
{
    public enum StatIncreaseFlags : ushort
    {
        Strength = 0x0001,
        Magic = 0x0002,
        Defense = 0x0004,
        MagicDefense = 0x0008,
        Agility = 0x0010,
        Luck = 0x0020,
        Evasion = 0x0040,
        Accuracy = 0x0080,
        HP = 0x0100,
        MP = 0x0200,
        IncreasePhysical = 0x0400,
        IncreaseMagic = 0x0800,
        ReducePhysical = 0x1000,
        ReduceMagic = 0x2000
    }
}