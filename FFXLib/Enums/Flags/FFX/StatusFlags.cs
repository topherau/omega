﻿using System;
using FFXLib.Classes;
using Newtonsoft.Json;

namespace FFXLib.Enums.Flags.FFX
{
    [Flags]
    [JsonConverter(typeof(JsonFlagConverter<StatusFlags>))]
    public enum StatusFlags : ushort
    {
        Scan = 0x0001,
        DistilPower = 0x0002,
        DistilMagic = 0x0004,
        DistilSpeed = 0x0008,
        Flag4 = 0x0010,
        DistilAbility = 0x0020,
        Shield = 0x0040,
        Boost = 0x0080,
        Eject = 0x0100,
        AutoLife = 0x0200,
        Curse = 0x0400,
        Defend = 0x0800,
        Guard = 0x1000,
        Sentinel = 0x2000,
        Doom = 0x4000,
        Flag15 = 0x8000
    }
}