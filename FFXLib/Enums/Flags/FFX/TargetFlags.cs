﻿using System;
using FFXLib.Classes;
using Newtonsoft.Json;

namespace FFXLib.Enums.Flags.FFX
{
    [Flags]
    [JsonConverter(typeof(JsonFlagConverter<TargetFlags>))]
    public enum TargetFlags : ushort
    {
        TargetAllowed = 0x00000001,
        TargetEnemy = 0x00000002,
        TargetAll = 0x00000004,
        TargetOnlySelf = 0x00000008,
        TargetUnknown = 0x00000010,
        TargetOther = 0x00000020,
        TargetDead = 0x00000040,
        LongRange = 0x00000080
    }
}