﻿namespace FFXLib.Enums.Flags.FFX
{
    public enum SpecialEffectFlags1 : uint
    {
        Sensor = 0x00000001,
        FirstStrike = 0x00000002,
        Initiative = 0x00000004,
        Counterattack = 0x00000008,
        EvadeAndCounter = 0x00000010,
        MagicCounter = 0x00000020,
        MagicBooster = 0x00000040,
        Alchemy = 0x00000200,
        AutoPotion = 0x400,
        AutoMed = 0x800,
        AutoPhoenix = 0x1000,
        Piercing = 0x2000,
        HalfMPCost = 0x4000,
        OneMPCost = 0x8000,
        DoubleOverdrive = 0x10000,
        TripleOverdrive = 0x20000,
        SOSOverdrive = 0x40000,
        OverdriveToAP = 0x80000,
        DoubleAP = 0x100000,
        TripleAP = 0x200000,
        NoAP = 0x400000,
        Pickpocket = 0x800000,
        MasterThief = 0x1000000,
        BreakHPLimit = 0x2000000,
        BreakMPLimit = 0x4000000,
        BreakDamageLimit = 0x8000000,
        Gillionaire = 0x40000000,
        HPStroll = 0x80000000
    }
}