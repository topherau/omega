﻿using System;
using FFXLib.Classes;
using Newtonsoft.Json;

namespace FFXLib.Enums.Flags.FFX
{
    [Flags]
    [JsonConverter(typeof(JsonFlagConverter<BuffFlags>))]
    public enum BuffFlags : byte
    {
        Cheer = 0x01,
        Aim = 0x02,
        Focus = 0x04,
        Reflex = 0x08,
        Luck = 0x10,
        Jinx = 0x20
    }
}