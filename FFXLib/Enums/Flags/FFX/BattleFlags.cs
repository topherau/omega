﻿using System;
using FFXLib.Classes;
using Newtonsoft.Json;

namespace FFXLib.Enums.Flags.FFX
{
    [Flags]
    [JsonConverter(typeof(JsonFlagConverter<BattleFlags>))]
    public enum BattleFlags : uint
    {
        Absorb = 0x00000001,
        Steal = 0x00000002,
        RequiresUse = 0x00000004,
        RightMenu = 0x00000008,
        LeftMenu = 0x00000010,
        Delay1 = 0x00000020,
        Delay2 = 0x00000040,
        HitsRandomly = 0x00000080,
        IgnoreTough = 0x00000100,
        Silenceable = 0x00000200,
        Weapon = 0x00000400,
        Flag11 = 0x00000800,
        Flag12 = 0x00001000,
        Flag13 = 0x00002000,
        SelfDestruct = 0x00004000,
        Flag15 = 0x00008000,
        ChargeOverdrive = 0x00010000,
        DepleteOverdrive = 0x00020000,
        Flag18 = 0x00040000,
        Flag19 = 0x00080000,
        Flag20 = 0x00100000,
        Flag21 = 0x00200000,
        Overdrive = 0x00400000,
        Flag23 = 0x00800000,
        Physical = 0x001000000,
        Magic = 0x002000000,
        CanCritical = 0x004000000,
        Flag27 = 0x008000000,
        Restorative = 0x10000000,
        RemoveStatus = 0x20000000,
        Flag30 = 0x40000000,
        BreakDamageLimit = 0x80000000,
    }
}