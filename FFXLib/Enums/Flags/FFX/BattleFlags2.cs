﻿using System;
using FFXLib.Classes;
using Newtonsoft.Json;

namespace FFXLib.Enums.Flags.FFX
{
    [Flags]
    [JsonConverter(typeof(JsonFlagConverter<BattleFlags2>))]
    public enum BattleFlags2 : byte
    {
        CanUseInMenu = 0x01,
        Flag1 = 0x02,
        Flag2 = 0x04,
        Flag3 = 0x08,
        Flag4 = 0x10,
        Flag5 = 0x20,
        Flag6 = 0x40,
        Reflectable = 0x80,
    }
}