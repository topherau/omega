﻿namespace FFXLib.Enums.Flags.FFX
{
    public enum PermanentStatusFlags : uint
    {
        KO = 0x00000001,
        Zombie = 0x00000002,
        Petrify = 0x00000004,
        Poison = 0x00000008,
        PowerBreak = 0x00000010,
        MagicBreak = 0x00000020,
        ArmorBreak = 0x00000040,
        MentalBreak = 0x00000080,
        Confuse = 0x00000100,
        Berserk = 0x00000200,
        Provoke = 0x00000400,
        Threaten = 0x00000800,

        Sleep = 0x00010000,
        Silence = 0x00020000,
        Dark = 0x00040000,
        Shell = 0x00080000,
        Protect = 0x00100000,
        Reflect = 0x00200000,
        NulTide = 0x00400000,
        NulBlaze = 0x00800000,
        NulShock = 0x01000000,
        NulFrost = 0x02000000,
        Regen = 0x04000000,
        Haste = 0x08000000,
    }
}