﻿using System;
using FFXLib.Classes;
using Newtonsoft.Json;

namespace FFXLib.Enums.Flags.FFX
{
    [Flags]
    [JsonConverter(typeof(JsonFlagConverter<ElementalFlags>))]
    public enum ElementalFlags : byte
    {
        Fire = 0x01,
        Ice = 0x02,
        Lightning = 0x04,
        Water = 0x08,
        Holy = 0x10,
    }
}