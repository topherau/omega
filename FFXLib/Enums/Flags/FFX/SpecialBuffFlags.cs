﻿using System;
using FFXLib.Classes;
using Newtonsoft.Json;

namespace FFXLib.Enums.Flags.FFX
{
    [Flags]
    [JsonConverter(typeof(JsonFlagConverter<SpecialBuffFlags>))]
    public enum SpecialBuffFlags : byte
    {
        DoubleHP = 0x01,
        DoubleMP = 0x02,
        ZeroMPCost = 0x04,
        All9999 = 0x08,
        AllCrits = 0x10,
        Overdrive150 = 0x20,
        Overdrive200 = 0x40,
        Flag7 = 0x80
    }
}