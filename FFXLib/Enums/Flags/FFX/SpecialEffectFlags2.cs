﻿namespace FFXLib.Enums.Flags.FFX
{
    public enum SpecialEffectFlags2 : ushort
    {
        MPStroll = 0x1,
        NoEncounters = 0x2,
        Capture = 0x4
    }
}