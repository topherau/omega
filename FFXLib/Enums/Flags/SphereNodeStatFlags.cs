﻿using System;

namespace FFXLib.Enums.Flags
{
    [Flags]
    public enum SphereNodeStatFlags : ushort
    {
        Strength = 0x0001,
        Defense = 0x0002,
        Magic = 0x0004,
        MagicDefense = 0x0008,
        Agility = 0x0010,
        Luck = 0x0020,
        Evasion = 0x0040,
        Accuracy = 0x0080,
        HP = 0x0100,
        MP = 0x0200,
        Ability = 0x0400,
        Lock = 0x8000
    }
}