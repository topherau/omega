﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using FFXLib.Enums;
using FFXLib.Extension;
using FFXLib.FileSystem;
using FFXLib.FileSystem.Data;
using FFXLib.FileSystem.Data.Files;
using FFXLib.FileSystem.Data.Files.FFX;
using FFXLib.FileSystem.Data.Records;
using FFXLib.FileSystem.Data.Records.FFX.Kernel;
using FFXLib.Script;
using FFXLib.Script.Classes;
using FFXLib.Script.Decompiler;
using FFXLib.Script.Enums;
using Omega.FFX;

namespace OmegaTest
{
    class Program
    {
        private static FileManager _fm = new FileManager(Localization.German,
            "D:\\games\\steamapps\\common\\FINAL FANTASY FFX&FFX-2 HD REMASTER\\data\\FFX_Data.vbf", "D:\\dat\\mod");

        private static StringConverter _sc = _fm.StringConverter;

        static void Main(string[] args)
        {
            Record.StringConverter = _fm.StringConverter;

            TestDisassembly();
            Console.ReadLine();
        }

        private static void TestDisassembly()
        {
            Directory.CreateDirectory("ai");
            for (var i = 0; i < 361; i++)
            {
                var mf = new MonsterFile(_fm.GetStream($"ffx_ps2/ffx/master/jppc/battle/mon/_m{i:D3}/m{i:D3}.bin"));
                File.WriteAllBytes($"ai\\{i}.bin", mf.GetSegment(0));
            }
            
            //var sc = new ScriptDisassembler(mf.GetSegment(0));
            //var de = new ScriptDecompiler(sc);
            //Console.Write(de.GetFunction(2, 2));
            ////de.Decompile(1, 2);
        }

        private static void MacroDic()
        {
            var stream = _fm.GetStream("ffx_ps2/ffx/master/$LANG$/menu/macrodic.dcp");
            var blockStart = 0u;
            var blocks = new List<uint>();
            while (true)
            {
                if (blockStart != 0 && stream.Position >= blockStart)
                    break;
                var start = stream.ReadUInt32();
                if (start != 0 && blockStart == 0)
                    blockStart = start;

                blocks.Add(start);
            }

            var blockFiles = new List<StringTableFile>();
            for (var i = 0; i < blocks.Count; i++)
            {
                if (blocks[i] == 0)
                    continue;

                var blockEnd = 0u;
                for (var j = i + 1; j < blocks.Count; j++)
                {
                    if (blocks[j] == 0)
                        continue;

                    blockEnd = blocks[j];
                    break;
                }

                if (blockEnd == 0)
                {
                    blockEnd = (uint) stream.Length;
                }

                var blockLength = blockEnd - blocks[i];
                var bytes = stream.ReadBytes((int) blockLength);
                var st = new StringTableFile(new MemoryStream(bytes));
            }
        }

        private static void DoMonsters()
        {
            for (var i = 0; i < 361; i++)
            {
                var fileName = $"ffx_ps2/ffx/master/jppc/battle/mon/_m{i:D3}/m{i:D3}.bin";
                var monsterFile = new MonsterFile(_fm.GetStream(fileName));
                var bytes = monsterFile.GetSegment((int) MonsterFileSegment.Loot);
                File.WriteAllBytes($"m\\{i:D3}.bin", bytes);
            }
        }

        private static void TestEvent()
        {
            // Locate events
            var f = _fm.GetStream("ffx_ps2/ffx/proj/event/header/eventid.bin");

            var offsetList = new List<uint>();
            var lengthList = new List<uint>();

            offsetList.Add(f.ReadUInt32());
            lengthList.Add(f.ReadUInt32());

            while (f.Position < offsetList[0])
            {
                offsetList.Add(f.ReadUInt32());
                lengthList.Add(f.ReadUInt32());
            }

            var fieldList = new List<string>();
            for (var i = 0; i < offsetList.Count; i++)
            {
                f.Seek(offsetList[i], SeekOrigin.Begin);
                var str = f.ReadBytes((int) lengthList[i]);
                fieldList.Add(Encoding.ASCII.GetString(str).TrimEnd('\0'));
            }
        }

        private static void StringDump(string file)
        {
            var _stream = _fm.GetStream(file);

            // Return to beginning of stream
            _stream.Seek(0, SeekOrigin.Begin);

            // Read header
            var header = _stream.ReadUInt32();
            if (header != 1)
                throw new Exception("Invalid kernel file (wrong header)");

            _stream.Seek(6, SeekOrigin.Current);
            var _lastRecordIndex = _stream.ReadUInt16();
            var _recordLength = _stream.ReadUInt16();
            var _dataLength = _stream.ReadUInt16();
            var _dataStart = _stream.ReadUInt32();
            var _stringTable = new byte[0];

            var recordCount = _dataLength / _recordLength;

            // Read strings from file
            var stringTablePos = _dataLength + _dataStart;
            var stringTableLength = (int) (_stream.Length - stringTablePos);
            if (stringTableLength > 0)
            {
                _stream.Seek(stringTablePos, SeekOrigin.Begin);
                _stringTable = _stream.ReadBytes(stringTableLength);

                var offset = 0;
                while (offset < _stringTable.Length)
                {
                    var str = _sc.Decode(_stringTable, offset);
                    offset += str.Length + 1;
                    File.AppendAllText(file + ".txt", str + '\n');
                }
            }
            else
            {
                _stringTable = new byte[0];
            }
        }
    }
}